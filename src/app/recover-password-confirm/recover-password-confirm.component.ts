import { Component } from '@angular/core';

@Component({
  selector: 'app-recover-password-confirm-page',
  templateUrl: './recover-password-confirm.component.html',
  styleUrls: ['./recover-password-confirm.component.scss']
})
export class RecoverPasswordConfirmPageComponent { }
