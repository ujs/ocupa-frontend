import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from './services/auth.service';

import * as Raven from 'raven-js';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {

  constructor(private auth: AuthService, private cookieService: CookieService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authRequest = request;
    const token = this.auth.getToken();

    // By default, do not allow cookies to be sent
    authRequest = authRequest.clone({withCredentials: false});

    // If there is an API key stored, send it in this request
    if (token) {
      authRequest = authRequest.clone({headers: authRequest.headers.set('Content-Type', 'application/json')});
      if (!request.url.includes('viacep.com.br')) {
        authRequest = authRequest.clone({headers: authRequest.headers.set('Authorization', 'Token ' + token)});
      }
    } else {
      // If there is no key, allow cookies to be sent, but only if this is a request for an API key
      if (authRequest.url.includes('/profile/key')) {
        authRequest = authRequest.clone({withCredentials: true});
      }
    }

    // Send a csrftoken header, if the data is available as a cookie
    const csrftoken = this.cookieService.get('csrftoken');
    if (csrftoken && !request.url.includes('viacep.com.br')) {
      authRequest = authRequest.clone({ headers: authRequest.headers.set('X-CSRFToken', csrftoken) });
    }

    return next.handle(authRequest).catch(error => {
      // HTTP errors must be handled diferently for Sentry
      if (error instanceof HttpErrorResponse) {
        Raven.captureMessage(error.message, {
          level: 'warning',
          extra: { response_body: error.error }
        });
      }

      if (error.status === 401 || error.status === 403) {
        AuthService.logout.emit();
      }

      return Observable.throw(error);
    });
  }
}
