import { Component, OnInit } from '@angular/core';
import { Candidate } from '../models/candidate';
import { CandidateService } from '../services/candidate.service';

@Component({
    selector: 'app-candidates',
    templateUrl: './candidates.component.html',
    styleUrls: ['./candidates.component.scss'],
    providers: [ CandidateService ]
})
export class CandidatesComponent implements OnInit {

    public candidates: Array<Candidate>;

    constructor(public candidateService: CandidateService) { }

    ngOnInit() {
        this.candidateService.getCandidates().subscribe( (response: Array<Candidate> ) => this.candidates = response, error => {});
    }

}
