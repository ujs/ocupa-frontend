import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DoacaoComponent } from './doacao/doacao.component';
import { ProfilePageComponent } from './profile/profile.component';
import { ProfileEditPageComponent } from './profile-edit/profile-edit.component';
import { RegisterPageComponent } from './register/register.component';
import { LoginPageComponent } from './login/login.component';
import { RecoverPasswordPageComponent } from './recover-password/recover-password.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SubhomeManuComponent } from './subhome-manu/subhome-manu.component';
import { PageComponent } from './page/page.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RecoverPasswordConfirmPageComponent} from './recover-password-confirm/recover-password-confirm.component';
import { UJSAffiliationFacadeComponent } from './ujs/affiliation-facade/ujs-affiliation-facade.component';
import { UJSAffiliationFormComponent } from './ujs/affiliation-form/ujs-affiliation-form.component';
import { UJSLoginComponent } from './ujs/login/ujs-login.component';
import { UJSNewAffiliationComponent } from './ujs/new-affiliation/ujs-new-affiliation.component';
import { UJSPostAffiliationComponent } from './ujs/post-affiliation/ujs-post-affiliation.component';
import { VerifyEmailPageComponent } from './verify-email/verify-email.component';
import { DiscussionComponent } from './discussion/discussion.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { ProfileTwitterConnectComponent } from './users/profile-connect/profile-connect.component';

export const rootRouterConfig: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'entrar', component: LoginPageComponent },
  { path: 'doacao', component: DoacaoComponent },
  { path: 'perfil', component: ProfilePageComponent },
  { path: 'perfil/editar', component: ProfileEditPageComponent },
  { path: 'perfil/editar/twitter/connect/callback', component: ProfileTwitterConnectComponent },
  { path: 'registrar', component: RegisterPageComponent },
  { path: 'verificar-email/:key', component: VerifyEmailPageComponent},
  { path: 'recuperar-senha', component: RecoverPasswordPageComponent },
  { path: 'recuperar-senha/:uid/:token', component: RecoverPasswordConfirmPageComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'manu', component: SubhomeManuComponent },
  { path: 'pre-candidatos', component: CandidatesComponent },
  { path: 'afiliacao', component: UJSAffiliationFacadeComponent },
  { path: 'afiliacao/entrar', component: UJSLoginComponent },
  { path: 'afiliacao/formulario', component: UJSAffiliationFormComponent },
  { path: 'nova-afiliacao', component: UJSNewAffiliationComponent },
  { path: 'pos-afiliacao', component: UJSPostAffiliationComponent },
  { path: 'forum', component: DiscussionComponent },
  { path: 'sobre', children: [
    { path: '**', component: PageComponent },
  ]},
  { path: '**', component: NotFoundComponent }
];
