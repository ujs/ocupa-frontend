import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { InvisibleReCaptchaComponent } from 'ngx-captcha';
import { LocalStorageService } from 'ngx-webstorage';

import { City, Region } from '../../models/address';
import { Profile } from '../../models/profile';

import { AddressService } from '../../services/address.service';
import { AuthService } from '../../services/auth.service';
import { UJSAffiliationService } from '../../services/ujs-affiliation.service';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-ujs-affiliation-form',
  templateUrl: './ujs-affiliation-form.component.html',
  styleUrls: ['./ujs-affiliation-form.component.scss']
})
export class UJSAffiliationFormComponent implements OnInit {

  profile: Profile | any;
  profileExisted: boolean;

  form_errors: any = {};

  city: string;

  captcha = '';
  @ViewChild('recaptcha') recaptcha: InvisibleReCaptchaComponent;

  citiesOptions: City[];
  statesOptions: Region[];

  phoneMask = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(
    private addressService: AddressService,
    private authService: AuthService,
    private affiliationService: UJSAffiliationService,
    private changeDetector: ChangeDetectorRef,
    private localStorage: LocalStorageService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.profile = new Profile();
    const storedProfile = this.localStorage.retrieve('currentProfile');
    if (storedProfile) {
      this.profile = storedProfile;
      if (!this.profile.mailing_profile) {
        this.profile.mailing_profile = {active: false};
      }
      this.profileExisted = true;
    } else {
      this.profile.origin = 'affiliation';
      this.profile.mailing_profile = {active: false};
      this.route.queryParams.subscribe(params => {
        this.profile.email = params['email'];
        this.updateUsername();
      });
      this.profileExisted = false;
    }
    this.profile.congress_subscription = true;
    this.profile.ujs_affiliate = true;
    this.addressService.getRegions().subscribe(states => {
      this.statesOptions = states;
      if (this.profile.state) {
        this.addressService.getCitiesByRegion(this.profile.state).subscribe(cities => {
          this.citiesOptions = cities;
          this.searchCity();
        }, error => {});
      }
    }, error => {});
  }

  searchCitiesByState() {
    this.addressService.getCitiesByRegion(this.profile.state).subscribe(cities => {
      this.citiesOptions = cities;
    }, error => {});
  }

  searchCity() {
    if (this.profile.city) {
      this.addressService.getCityById(this.profile.city).subscribe(city => {
        this.city = city.name;
      }, error => {});
    }
  }

  markAsHuman(response: string) {
    this.captcha = response;
    this.submit();
  }

  verifyRobot() {
    if (this.validateForm()) {
      this.recaptcha.execute();
    }
  }

  updateUsername() {
    this.profile.username = this.profile.email;
  }

  validateForm(): boolean {
    this.form_errors = {};
    if (!this.profile.name) {
      this.form_errors.name = ['Campo obrigatório'];
    }
    if (!this.profile.birth_date) {
      this.form_errors.birth_date = ['Campo obrigatório'];
    }
    if (!this.profileExisted) {
      if (!this.profile.email) {
        this.form_errors.email = ['Campo obrigatório'];
      }
      if (!this.profile.password1) {
        this.form_errors.password1 = ['Campo obrigatório'];
      }
      if (!this.profile.password2) {
        this.form_errors.password2 = ['Campo obrigatório'];
      }
      if (this.profile.password1 !== this.profile.password2) {
        this.form_errors.password2 = ['As senhas não batem'];
      }
    }
    if (!this.profile.state) {
      this.form_errors.state = ['Campo obrigatório'];
    }
    if (!this.profile.city) {
      this.form_errors.city = ['Campo obrigatório'];
    }
    if (!this.profile.phone_number) {
      this.form_errors.phone_number = ['Campo obrigatório'];
    }
    if (this.profile.studies == null) {
      this.form_errors.studies = ['Campo obrigatório'];
    }
    if (this.profile.studies && this.profile.school === '') {
      this.form_errors.school = ['Campo obrigatório'];
    }
    if (this.profile.is_working == null) {
      this.form_errors.is_working = ['Campo obrigatório'];
    }
    if (this.profile.is_working && this.profile.workplace === '') {
      this.form_errors.workplace = ['Campo obrigatório'];
    }
    if (this.profile.has_mandate == null) {
      this.form_errors.has_mandate = ['Campo obrigatório'];
    }
    if (this.profile.has_mandate && !this.profile.mandate) {
      this.form_errors.mandate = ['Campo obrigatório'];
    }
    if (this.profile.ujs_affiliate){
      if (!this.profile.accepted_statute) {
        this.form_errors.accepted_statute = ['É obrigatório aceitar o estatuto da UJS e programa socialista'];
      }
      if (!this.profile.accepted_terms) {
        this.form_errors.accepted_terms = ['É obrigatório aceitar os termos de uso e política de privacidade'];
      }
    }
    if (!this.objectEmpty(this.form_errors)) {
      this.form_errors.submit_button = ['Por favor, verifique os erros no formulário'];
    }
    return this.objectEmpty(this.form_errors);
  }

  submit() {
    this.affiliationService[this.profileExisted ? 'update' : 'create'](this.profile, this.captcha).subscribe(profile => {
      this.profile = profile;
      this.authService.getUserStatus(this.profile.email).subscribe(status => {
        this.router.navigate(['nova-afiliacao']);
      }, error => {});
    }, errors => {
      if ('email' in errors.error && errors.error.email
        .includes('Um usuário já foi registado com este endereço de e-mail.')) {
          // TODO add some login handler
      }
      this.form_errors = errors.error;
      this.changeDetector.detectChanges();
    });
   }

  objectEmpty(obj: object) {
    if (!obj) {
      return false;
    }
    return Object.keys(obj).length === 0;
  }

}
