import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UJSAffiliationFormComponent } from './ujs-affiliation-form.component';

describe('ProfileEditComponent', () => {
  let component: UJSAffiliationFormComponent;
  let fixture: ComponentFixture<UJSAffiliationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UJSAffiliationFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UJSAffiliationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
