import { Component, OnInit, EventEmitter, ViewChild, HostListener, Input } from '@angular/core';
import { Router } from '@angular/router';

import { LocalStorageService } from 'ngx-webstorage';

import { ProfileService } from '../../services/profile.service';
import { AuthService } from '../../services/auth.service';
import { Profile } from '../../models/profile';

@Component({
  selector: 'app-ujs-login',
  templateUrl: './ujs-login.component.html',
  styleUrls: ['./ujs-login.component.scss'],
})
export class UJSLoginComponent {

  user: any;
  formErrors: any;

  @ViewChild('passwordErrors') passwordErrors;
  @ViewChild('emailErrors') emailErrors;

  constructor(
    private authService: AuthService,
    private localStorage: LocalStorageService,
    private profileService: ProfileService,
    private router: Router
    ) {

    this.user = new Profile();
    this.user.email = this.localStorage.retrieve('ujs_email');
    AuthService.loginSuccessNext = 'afiliacao/formulario';
  }

  login() {
    this.authService.signIn(this.user).subscribe(
      response => {},
      error => {
        this.formErrors = error.error;
        if (this.formErrors.non_field_errors) {
          if (this.formErrors.non_field_errors
            .includes('E-mail is not verified.')) {
            this.formErrors.non_field_errors = ['Seu e-mail ainda não foi verificado. '
              + 'Mandamos uma mensagem para o seu endereço com instruções para você acessar sua conta'];
          }
        }
      });
  }
}
