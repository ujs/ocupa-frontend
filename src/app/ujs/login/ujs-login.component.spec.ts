import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UJSLoginComponent } from './ujs-login.component';

describe('LoginComponent', () => {
  let component: UJSLoginComponent;
  let fixture: ComponentFixture<UJSLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UJSLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UJSLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
