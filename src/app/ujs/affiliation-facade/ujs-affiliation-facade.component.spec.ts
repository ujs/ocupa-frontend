import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UJSAffiliationFacadeComponent } from './ujs-affiliation-facade.component';

describe('UJSAffiliationFacadeComponent', () => {
  let component: UJSAffiliationFacadeComponent;
  let fixture: ComponentFixture<UJSAffiliationFacadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UJSAffiliationFacadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UJSAffiliationFacadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
