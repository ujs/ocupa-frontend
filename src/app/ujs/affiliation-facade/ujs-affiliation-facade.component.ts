import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../services/auth.service';
import { SocialFacebookService } from '../../services/social-facebook.service';
import { Router } from '@angular/router';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-ujs-affiliation-facade',
  templateUrl: './ujs-affiliation-facade.component.html',
  styleUrls: ['./ujs-affiliation-facade.component.scss']
})
export class UJSAffiliationFacadeComponent implements OnInit {

  user: any = {};
  formErrors: any = {};
  socialErrors: any = {};

  constructor(
    private authService: AuthService,
    private profileService: ProfileService,
    private socialFacebookService: SocialFacebookService,
    private router: Router,
  ) { }

  ngOnInit() {
    if (this.profileService.get()) {
      this.authService.destroy();
    }
    AuthService.loginSuccessNext = 'afiliacao/formulario';
  }

  checkUserStatus() {
    this.authService.getAffiliationUserStatus(this.user.email).subscribe(
      user => {
        if(user.exists) {
          this.user.exists = true;
        } else {
          this.router.navigate([AuthService.loginSuccessNext], {queryParams: {email: this.user.email}});
        }
      }
    )
  }

  login() {
    this.authService.signIn(this.user).subscribe(
      response => {
      },
      error => {
        this.formErrors = error.error;
        if (this.formErrors.non_field_errors) {
          if (this.formErrors.non_field_errors
            .includes('E-mail is not verified.')) {
            this.formErrors.non_field_errors = ['Seu e-mail ainda não foi verificado. '
              + 'Mandamos uma mensagem para o seu endereço com instruções para você acessar sua conta'];
          }
        }
      });
  }

  loginWithFacebook() {
    this.socialFacebookService.login();

    this.socialFacebookService.loginReturn.subscribe((data) => {
      if (data.error) {
        // tslint:disable-next-line:max-line-length
        this.socialErrors = 'Já existe um usuário registrado com o seu email do Facebook. Clique em "Esqueci minha senha" para acessar sua conta';
      }
    }, error => {
      // handle request errors here
      this.socialErrors = error.error;
    });
  }

}
