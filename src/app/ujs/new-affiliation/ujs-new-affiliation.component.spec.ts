import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UJSNewAffiliationComponent } from './ujs-new-affiliation.component';

describe('UJSNewAffiliationComponent', () => {
  let component: UJSNewAffiliationComponent;
  let fixture: ComponentFixture<UJSNewAffiliationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UJSNewAffiliationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UJSNewAffiliationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
