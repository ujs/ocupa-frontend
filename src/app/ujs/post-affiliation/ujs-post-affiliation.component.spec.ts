import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UJSPostAffiliationComponent } from './ujs-post-affiliation.component';

describe('UjsPostAffiliationComponent', () => {
  let component: UJSPostAffiliationComponent;
  let fixture: ComponentFixture<UJSPostAffiliationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UJSPostAffiliationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UJSPostAffiliationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
