import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'page-header',
  templateUrl: './page-header.component.html',
  styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {

  @Output() onPlayVideo: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  @Input() color: String = '';
  @Input() category: String = '';
  @Input() hasVideo: String = '';
  @Input() bgImage: String = '/assets/images/page-header-pattern.png';
  @Input() title: String = '';
  public playing = false;

  constructor() { }

  ngOnInit() {
  }

  playVideo() {
    this.playing = true;
    this.onPlayVideo.emit(this.playing);
  }

}
