import { Component, OnInit } from '@angular/core';
import { ConversationService } from '../conversations/conversation/conversation.service';
import { Conversation } from '../conversations/conversation/conversation.model';
import * as _ from 'lodash';

@Component({
  selector: 'app-subhome-manu',
  templateUrl: './subhome-manu.component.html',
  styleUrls: ['./subhome-manu.component.scss'],
  providers: [ConversationService],
})
export class SubhomeManuComponent implements OnInit {
  user: any = {};

  conversations: Conversation[];

  player: any;
  videoModalActive = false;
  videoModalConversation: any;

  constructor(
    private conversationService: ConversationService,
  ) { }

  ngOnInit() {
    this.conversationService.list().subscribe((conversations: Conversation[]) => {
      this.conversations = conversations;
    }, error => {
      console.log(error);
    });
  }

  backgroundImage(conversation: Conversation): string {
    const imagem_path = (_.isNil(conversation.background_image)) ? '/assets/theme/card-bg.jpg' : conversation.background_image;
    return imagem_path;
  }

  controlVideo(action, conversation) {
    this.videoModalActive = action === 'play';
    this.videoModalConversation = conversation;
  }

  savePlayer(player) {
    this.player = player;

    this.player[(this.videoModalActive ? 'play' : 'pause') + 'Video']();
  }

  onStateChange(event) {
    if (event.data === 0) {
      this.videoModalActive = false;
    }
  }

}
