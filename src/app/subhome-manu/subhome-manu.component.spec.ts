import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubhomeManuComponent } from './subhome-manu.component';

describe('SubhomeManuComponent', () => {
  let component: SubhomeManuComponent;
  let fixture: ComponentFixture<SubhomeManuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubhomeManuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubhomeManuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
