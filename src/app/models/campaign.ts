import { PaymentMethod, PaymentType } from './checkout';

export class Campaign {
    id: number;
    suggested_values: number[];
    name: string;
    reference: string; /* for PagSeguro */
    description: string;
    goal: number;
    raised: number;
    donations_count: number;
    slug: string;
    payment_methods: PaymentMethod[];
    payment_types: PaymentType[];
    rubric: string[];
}
