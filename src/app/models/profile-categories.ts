export class ActivismSchoolWorkplace {
  name: string;
  id: number;
}

export class PoliticalFront {
  name: string;
  id: number;
}

export class UJSManagerRole {
  name: string;
  id: number;
}
