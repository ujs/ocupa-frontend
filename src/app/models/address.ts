export class Address {
  bairro: string;
  cep: string;
  complemento: string;
  gia: string;
  ibge: string;
  localidade: string; /* City name */
  logradouro: string;
  unidade = '';
  uf: string;
}

export class City {
  id: number;
  name: string;
  name_ascii: string;
  alternate_names: string;
  display_name: string;
  search_names: string;
  latitude: string;
  longitude: string;
  population: number;
  feature_code: string;
  timezone: string;
  region: number;
  country: number;
}

export class Region {
  id: number;
  name: string;
  name_ascii: string;
  geoname_id: number;
  alternate_names: string;
  display_name: string;
  geoname_code: string;
  country: number;
}
