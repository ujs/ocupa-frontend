import { PaymentType } from './checkout';

export interface Subscription {
    name: string;
    value: number;
    start_date: string;
    end_date: string;
    period: PaymentType;
    active: boolean;
    status: number;
    pre_approval_plan: number;
    campaign: {
        name: string;
        slug: string;
    };
}

export enum TransactionType {
    Donation = 'donation',
    Registration = 'registration'
}

export interface Transaction {
    amount: number;
    campaign: {
      name: string;
      slug: string;
    };
    start_date: string;
    type: TransactionType;
    status: number;
}

export class Profile {
    public id: number;
    public token: string;
    public image: any;
    public email: string;
    public username: string;
    public name: String;
    public cpf: string;
    public race: string|null = null;
    public gender: string|null = null;
    public gende_other: string|null = null;

    public sexual_orientation: string|null = null;
    public sexual_orientation_other: string|null = null;

    public birth_date: string;
    public biography: string;

    // Contact info
    public phone_number: string;
    public phone: {
        areaCode: string,
        number: string,
    };

    // Location info
    public address: string;
    public address_number: string;
    public address_extra: string;
    public neighborhood: string;
    public city: number;
    public state: number;
    public country: string;
    public zipcode: string;

    // Academic info
    public studies: boolean|null = null;
    public school_state = '';
    public school_city = '';
    public school = '';

    //Work info
    public is_working: boolean|null = null;
    public workplace = '';

    // Political info
    public has_vote: boolean|null = null;
    public vote_city: string;
    public has_mandate: boolean|null = null;
    public mandate: string;
    public ujs_manager_role: string[];
    public activism_place: string|null = null;
    public activism_school_workplace: number[];
    public activism_school_workplace_other: string;
    public political_front: number[];
    public political_front_other: string;

    // Affiliation info
    public ujs_affiliate = false;
    public origin = 'signup';

    public subscriptions: Subscription[];
    public transactions: Transaction[];

    public is_superuser: boolean;
    public is_staff: boolean;
}

export class ProfileStatus {
    public user: string;
    public active: boolean;
    public exists: boolean;
    public ujs_affiliate: boolean|null;
}
