import { Profile } from './profile';
import { Campaign } from './campaign';

export class Item {
    id: string;
    description: string;
    quantity: number;
    amount: number;

    constructor (id: string, description: string, amount: number,
        quantity: number) {
            this.id = id;
            this.description = description;
            this.quantity = quantity;
            this.amount = amount;
    }
}

export class Payment {
    payment_type: PaymentType;
    holder_name: string;
    holder_cpf: string;
    holder_birth_date: string;

    brand: string;
    card_number: string;
    card_cvv: string;
    expiration_date: string;

    street: string;
    number: number;
    complement: string;
    district: string;
    postal_code: string;
    city: string;
    state: string;
    state_uf: string;
    country: string;

    constructor() {
    }
}

export enum PaymentMethod {
    Credito = 'creditCard',
    Debito = 'eft',
    Boleto = 'boleto',
}

export enum PaymentType {
    Weekly = 'weekly',
    Monthly = 'monthly',
    Single = 'single'
}

export class PagSeguroCheckoutSession {
    session_id: string;
    card_token: string;
    sender_hash: string;
    is_term_accepted: boolean;

    reference: string;
    campaign: string;
    rubric: string;

    method: PaymentMethod;
    payment_type: PaymentType;
    sender: Profile;
    payment: Payment;

    items: Array<Item>;

    status: any;

    constructor(sender: Profile, reference: string) {
        this.sender = sender;
        this.status = {};
        this.items = new Array<Item>();
        this.reference = reference;
    }

    public calculateAmount() {
        let amount = 0;
        this.items.forEach(i => {
            amount = amount + i.quantity.valueOf() * i.amount.valueOf();
        });
        return amount;
    }

    public setPaymentMethod(method: PaymentMethod) {
        this.method = method;
        if (method === PaymentMethod.Credito) {
            this.payment = new Payment();
        }
    }
}

export class PagSeguroCheckout {
    transaction: string;
    payment_method: PaymentMethod;
    status: number;
    value: number;
    created_at: Date;
    updated_at: Date;
    sender: Profile;
    campaign: Campaign;
}
