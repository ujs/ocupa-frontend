export class StaticPage {
  id: number;
  category?: string; /* Not currently implemented. */
  content: string;
  title: string;
  url: string;
}
