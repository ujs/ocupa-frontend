
export class SocialMedias {
    facebook: String;
    instagram: String;
    twitter: String;
    flickr: String;
    email: String;
    site: String;
    whatsapp: String;

    constructor(facebook = '', instagram = '', email = '',  whatsapp = '', twitter = '', site = '', flickr = '') {
        this.facebook = facebook;
        this.instagram = instagram;
        this.twitter = twitter;
        this.flickr = flickr;
        this.email = email;
        this.site = site;
        this.whatsapp = whatsapp;
    }
}
export class Candidate {
    name: String;
    image: String;
    party: String;
    city: String;
    role: String;
    description: String;
    social_medias: SocialMedias;

    constructor(name = '', image = '', social_medias = new SocialMedias(), description = '', party = '', city = '', role = '') {
        this.name = name;
        this.image = image;
        this.party = party;
        this.city = city;
        this.role = role;
        this.description = description;
        this.social_medias = social_medias;
    }
}
