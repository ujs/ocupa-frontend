import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'paragraphs',
  pure: true
})
export class ParagraphsPipe implements PipeTransform {

  transform(input: string): string {
    if (!input) {
      return input;
    }
    return input.split('\n')
                .map(line => `<p>${line}</p>`)
                .filter(line => line !== '<p></p>')
                .join('');
  }
}
