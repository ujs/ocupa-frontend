import { Component, Input, OnInit } from '@angular/core';

import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {

  email: string;
  formErrors: any;
  message: any;

  @Input() signupPath = '/registrar';

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  reset() {
    this.authService.passwordReset({ email: this.email.toLowerCase() })
      .subscribe(
        data => this.message = data,
        error => this.formErrors = error
      );
  }

}
