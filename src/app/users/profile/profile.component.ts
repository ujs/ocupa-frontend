import { Component, OnInit } from '@angular/core';
import { PaymentType } from '../../models/checkout';
import { Profile, TransactionType } from '../../models/profile';
import { AddressService } from '../../services/address.service';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  profile: Profile;

  personal_progress = 0.0;
  activism_progress = 0.0;
  fronts_progress = 0.0;

  city: string;

  PaymentType = PaymentType;
  TransactionType = TransactionType;

  constructor(private addresService: AddressService, private profileService: ProfileService) { }

  ngOnInit() {
    this.profile = this.profileService.get() || new Profile();
    if (this.profile.city) {
      this.addresService.getCityById(this.profile.city).subscribe(response => {
        this.city = response.name;
      }, error => {});
    }
    this.personal_progress = this.profileService.computePersonalProgress(this.profile);
    this.activism_progress = this.profileService.computeActivismProgress(this.profile);
    this.fronts_progress = this.profileService.computeFrontsProgress(this.profile);
  }

}
