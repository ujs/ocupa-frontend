import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Address, City, Region } from '../../models/address';
import { Profile } from '../../models/profile';

import { ProfileService } from '../../services/profile.service';
import { ProfileCategoriesService } from '../../services/profile-categories.service';
import { ActivismSchoolWorkplace, PoliticalFront, UJSManagerRole } from '../../models/profile-categories';
import { AddressService } from '../../services/address.service';
import { SocialFacebookService } from '../../services/social-facebook.service';
import { AuthService } from '../../services/auth.service';
import { SocialTwitterService } from '../../services/social-twitter.service';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.scss']
})
export class ProfileEditComponent implements OnInit {

  profile: Profile;
  password = '';
  new_password = '';

  personal_progress = 0.0;
  activism_progress = 0.0;
  fronts_progress = 0.0;

  city: string;
  has_zipcode = false;
  socialErrors: any;
  socialAccounts: any;
  allConnected = false;

  genderOptions = [
    { id: 'CIS_FEMALE', name: 'Mulher Cis' },
    { id: 'CIS_MALE', name: 'Homem Cis' },
    { id: 'TRANSGENDERED', name: 'Transgênero' },
    { id: 'TRANSSEXUAL_WOMAN', name: 'Mulher Transexual' },
    { id: 'TRANSSEXUAL_MAN', name: 'Homem Transexual' },
    { id: 'DO_NOT_KNOW', name: 'Não sei' },
    { id: 'OTHER', name: 'Outro' },
  ];

  sexualOrientationOptions = [
    { id: 'LESBICA', name: 'Lesbian' },
    { id: 'GAY', name: 'Gay' },
    { id: 'BISSEXUAL', name: 'Bissexual' },
    { id: 'HETEROSSEXUAL', name: 'Heterossexual' },
    { id: 'ASSEXUAL', name: 'Assexual' },
    { id: 'FLUIDO', name: 'Fluid' },
    { id: 'OTHER', name: 'Other' }
  ];

  raceColorOptions = [
    { id: 'BLACK', name: 'Preta' },
    { id: 'BROWN', name: 'Parda' },
    { id: 'WHITE', name: 'Branca' },
    { id: 'YELLOW', name: 'Amarela' },
    { id: 'INDIGENOUS', name: 'Indígena' },
    { id: 'DO_NOT_KNOW', name: 'Não sei' },
    { id: 'UNDECLARED', name: 'Não declarada' },
  ];

  activismPlaceOptions = [
    { id: 'residential_area', name: 'Local de moradia' },
    { id: 'school', name: 'Local de estudo' },
    { id: 'workplace', name: 'Local de trabalho' },
  ];

  activismSchoolWorkplaceOptions: ActivismSchoolWorkplace[];
  politicalFrontsOptions: PoliticalFront[];
  managerRoleOptions: UJSManagerRole[];

  citiesOptions: City[];
  statesOptions: Region[];

  cepMask = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
  cpfMask = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  phoneMask = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(
    private addressService: AddressService,
    private profileService: ProfileService,
    private profileCategoriesService: ProfileCategoriesService,
    private router: Router,
    private authService: AuthService,
    private socialFacebookService: SocialFacebookService,
    private socialTwitterService: SocialTwitterService,
  ) { }

  ngOnInit() {
    this.profile = this.profileService.get() || new Profile();
    this.profileService.updateFromServer().subscribe(profile => {
      this.profile = profile;
      this.updateSocialAccountInfo();

      AuthService.connectSuccess.subscribe(data => {
        this.updateSocialAccountInfo()
      }, error => {console.log(error)});

      this.profile.ujs_manager_role = this.profile.ujs_manager_role || [];
      this.searchZipCode();
      this.searchCity();
      this.personal_progress = this.profileService.computePersonalProgress(this.profile);
      this.activism_progress = this.profileService.computeActivismProgress(this.profile);
      this.fronts_progress = this.profileService.computeFrontsProgress(this.profile);

      this.profileCategoriesService.getActivismSchoolWorkplace().subscribe(response => {
        this.activismSchoolWorkplaceOptions = response;
      }, error => {});
      this.profileCategoriesService.getPoliticalFronts().subscribe(response => {
        this.politicalFrontsOptions = response;
      }, error => {});
      this.profileCategoriesService.getUJSManagerRoles().subscribe(response => {
        this.managerRoleOptions = response;
      }, error => {});

      this.addressService.getRegions().subscribe(states => {
        this.statesOptions = states;
        if (this.profile.state) {
          this.addressService.getCitiesByRegion(this.profile.state).subscribe(cities => {
            this.citiesOptions = cities;
          }, error => {});
        }
      }, error => {});
    }, error => {});
  }

  clearAddress() {
    this.has_zipcode = false;
    this.profile.state = null;
    this.profile.city = null;
  }

  searchCity() {
    if (this.profile.city) {
      this.addressService.getCityById(this.profile.city).subscribe(city => {
        this.city = city.name;
      }, error => {});
    }
  }

  searchZipCode() {
    if (!this.profile.zipcode || this.profile.zipcode.length === 0) {
      this.clearAddress();
    } else {
      this.profile.zipcode = this.profile.zipcode.trim().split('-').join('');
      this.addressService.fetchAddress(this.profile.zipcode).subscribe(response => {
        this.has_zipcode = true;
        this.addressService.getRegionByUF(response.uf).subscribe(states => {
          if (states && states[0]) {
            this.profile.state = states[0].id;
            this.city = response.localidade;
            this.addressService.getCitiesByRegion(this.profile.state).subscribe(cities => {
              this.citiesOptions = cities;
              this.profile.city = this.citiesOptions.find(city => city.name === this.city).id;
            }, error => {});
          } else {
            this.clearAddress();
          }
        }, error => {});
        this.profile.address = response.logradouro;
      }, error => {
        this.clearAddress();
      });
    }
    this.trackPersonalProgress();
  }

  trackPersonalProgress() {
    this.personal_progress = this.profileService.computePersonalProgress(this.profile);
  }

  trackActivismProgress() {
    this.activism_progress = this.profileService.computeActivismProgress(this.profile);
  }

  trackFrontsProgress() {
    this.fronts_progress = this.profileService.computeFrontsProgress(this.profile);
  }

  submit() {
    this.profileService.save(this.profile).subscribe(response => {
      this.profile = response;
      this.profileService.set(this.profile);
      this.router.navigate(['perfil']);
    }, error => {});
   }

  arrayContains(property: string, element): boolean {
    return this.profile[property].includes(element.id);
  }

  arrayToggle(property: string, element: {id}, insert: boolean): void {
    if (insert) {
      this.profile[property].push(element.id);
    } else {
      this.profile[property].splice(this.profile[property].map(x => x.id).indexOf(element.id), 1);
    }
  }

  connectToFacebook(){
    this.socialFacebookService.connect();
    this.socialFacebookService.connectReturn.subscribe((data) => {
      console.log(data);
      if (data.error) {
        // tslint:disable-next-line:max-line-length
        this.socialErrors = 'Erro ao conectar sua conta do Facebook. Tente mais tarde';
      }
    }, error => {
      // handle request errors here
      this.socialErrors = error.error;
    });
  }

  connectToTwitter() {
    this.socialTwitterService.getApiToken();
    this.socialTwitterService.tokenReturn.subscribe(oauthToken => {
      const windowRef: Window = window.open(
                                  `https://api.twitter.com/oauth/authorize?oauth_token=${oauthToken}`,
                                  'twitter-window',
                                  'menubar=false,toolbar=false');

      const that = this;
      const popupTick = setInterval(function() {
        if (windowRef && windowRef.closed) {
          clearInterval(popupTick);
          that.updateSocialAccountInfo();
        }
      }, 500);
    },
    error => {})
  }

  updateSocialAccountInfo() {
    this.profileService.getConnectedAccounts().subscribe((data: Array<any>) => {
      this.socialAccounts = {};
      this.allConnected = data.length === 2;
      data.forEach(acc => {
        this.socialAccounts[acc.provider] = acc;
      });
    }, error => {
      this.socialErrors = 'Não foi possível exibir Contas Sociais';
      this.allConnected = true;
    });
  }

}
