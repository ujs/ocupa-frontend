import { Component, OnInit, EventEmitter, ViewChild, HostListener, Input } from '@angular/core';
import { Router } from '@angular/router';

import * as _ from 'lodash';

import { ProfileService } from '../../services/profile.service';
import { AuthService } from '../../services/auth.service';
import { SocialFacebookService } from '../../services/social-facebook.service';
import { Profile } from '../../models/profile';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {

  user: any;
  socialErrors: any;
  formErrors: any;

  @Input() signupPath = '/registrar';
  @Input() recoverPasswordPath = '/recuperar-senha';
  @Input() socialLogin = true;

  @ViewChild('passwordErrors') passwordErrors;
  @ViewChild('emailErrors') emailErrors;

  constructor(private authService: AuthService,
              private profileService: ProfileService,
              private socialFacebookService: SocialFacebookService,
              private router: Router) {

    this.user = new Profile();
  }

  login() {
    this.user.email = this.user.email.toLowerCase();
    this.authService.signIn(this.user).subscribe(
      response => {},
      error => {
        this.formErrors = error.error;
        if (this.formErrors.non_field_errors) {
          if (this.formErrors.non_field_errors
            .includes('E-mail is not verified.')) {
            this.formErrors.non_field_errors = ['Seu e-mail ainda não foi verificado. '
              + 'Mandamos uma mensagem para o seu endereço com instruções para você acessar sua conta'];
          } else if (this.formErrors.non_field_errors
            .includes('User is inactive')) {
            const message = 'Seu perfil no Ocupa! ainda está inativo.'
              + ` <a href="${ this.recoverPasswordPath }">Clique aqui</a>`
              + ' para ativar seu usuário cadastrando uma senha';
            this.formErrors.non_field_errors = [message];
          }
        }
      });
  }

  loginWithFacebook() {
    this.socialFacebookService.login();

    this.socialFacebookService.loginReturn.subscribe((data) => {
      if (data.error) {
        // tslint:disable-next-line:max-line-length
        this.socialErrors = 'Já existe um usuário registrado com o seu email do Facebook. Clique em "Esqueci minha senha" para acessar sua conta';
      }
    }, error => {
      // handle request errors here
      this.socialErrors = error.error;
    });
  }

  loginWithTwitter() {
    const windowRef: Window = window.open(
                                '/accounts/twitter/login/?next=%2Fapi%2Fprofile%2Fclose',
                                'twitter-window',
                                'menubar=false,toolbar=false');

    const that = this;
    const popupTick = setInterval(function() {
      if (windowRef.closed) {
        clearInterval(popupTick);
        that.authService.getTokenFromServer().subscribe((data: any) => {
          AuthService.loginSuccess.emit(data);
        }, (error: any) => {
          this.socialErrors = 'Erro ao logar com o Twitter';
        });
      }
    }, 500);
  }

}
