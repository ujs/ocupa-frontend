import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {

  message = '';
  verification_finished = false;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.authService.verifyEmail({'key' : params['key']})
        .subscribe(data => {
          this.message = 'Seu e-mail foi verificado com sucesso! '
            + '<a href="/login">Fazer login</a> ';
          this.verification_finished = true;
        }, error => {
          this.message = 'Falha na verificação do seu e-mail';
          this.verification_finished = true;
        });
    });
  }

}
