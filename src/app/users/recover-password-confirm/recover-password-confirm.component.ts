import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ProfileService } from '../../services/profile.service';
import { Profile } from '../../models/profile';

@Component({
  selector: 'app-recover-password-confirm',
  templateUrl: './recover-password-confirm.component.html',
  styleUrls: ['./recover-password-confirm.component.scss']
})
export class RecoverPasswordConfirmComponent implements OnInit {

  confirmData: any = {};
  formErrors: any;
  message = undefined;

  @Input() signupPath = '/registrar';

  constructor(private route: ActivatedRoute,
    private authService: AuthService,
    private profileService: ProfileService) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.confirmData['uid'] = params['uid'];
      this.confirmData['token'] = params['token'];
    });
  }

  resetConfirm() {
    this.authService.passwordResetConfirm(this.confirmData).subscribe(
      data => {
        this.message = 'Senha recuperada com sucesso! '
          + '<a href="/login">Fazer login</a> ';
      },
      error => {
        this.formErrors = {};
        if ('new_password2' in error.error) {
          this.formErrors['new_password2'] = {};
          this.formErrors['new_password2']['isInvalid'] =
            error.error.new_password2[0];
        }
        if ('token' in error.error) {
          this.formErrors['non_field_errors'] = {};
          const msg = 'Os dados de confirmação são inválidos, ou já foram '
            + 'utilizados, caso queira recuperar sua senha '
            + '<a href="/recuperar-senha">Clique aqui</a> ';
          this.formErrors['non_field_errors']['isInvalid'] = msg;
        }
      }
    );
  }

}
