import { ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';

import { InvisibleReCaptchaComponent } from 'ngx-captcha';

import { AddressService } from '../../services/address.service';
import { AuthService } from '../../services/auth.service';
import { SocialFacebookService } from '../../services/social-facebook.service';

import { City, Region } from '../../models/address';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  cities: City[];
  regions: Region[];

  form = {
    accepted_terms: false,
    captcha: '',
    city: null as number,
    email: '',
    mailing_profile: {
      active: false
    },
    password1: '',
    password2: '',
    phone_number: '',
    state: null as number,
  };
  formErrors: any;
  socialErrors: any;
  register_success = false;

  @Input() loginPath = '/login';
  @Input() socialLogin = true;

  @ViewChild('recaptcha') recaptcha: InvisibleReCaptchaComponent;

  public phoneMask = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];

  constructor(
    private addressService: AddressService,
    private authService: AuthService,
    private changeDetector: ChangeDetectorRef,
    private socialFacebookService: SocialFacebookService,
  ) { }

  ngOnInit() {
    this.addressService.getRegions().subscribe(regions => {
      this.regions = regions;
    }, error => {});
  }

  getCitiesByState() {
    this.addressService.getCitiesByRegion(this.form.state).subscribe(cities => {
      this.cities = cities;
    }, error => {});
  }

  markAsHuman(response: string) {
    this.form.captcha = response;
    this.signUp();
  }

  verifyRobot() {
    this.recaptcha.execute();
  }

  signUp() {
    this.authService.signUp(this.form).subscribe(
      data => {
        this.register_success = true;
        this.changeDetector.detectChanges();
      },
      error => {
        this.formErrors = error.error;
        if (this.formErrors) {
          if (this.formErrors.email && this.formErrors.email.includes('Um usuário já foi registado com este endereço de e-mail.')) {
            this.formErrors.email = ['Você já tem usuário no Ocupa. <a href="/recuperar-senha">Clique aqui</a> para recuperar sua senha.'];
          }
          if (this.formErrors.captcha && this.formErrors.captcha.includes('Invalid CAPTCHA.')) {
            this.formErrors.captcha = ['CAPTCHA inválido. Não foi possível criar usuário.'];
          }
        }
        this.changeDetector.detectChanges();
      }
    );
  }

  signUpWithFacebook() {
    this.socialFacebookService.login();

    this.socialFacebookService.loginReturn.subscribe((data) => {
      if (data.error) {
        // tslint:disable-next-line:max-line-length
        this.socialErrors = 'Já existe um usuário registrado com o seu email do Facebook. Clique em "Esqueci minha senha" para acessar sua conta';
      }
    }, error => {
      // handle request errors here
      this.socialErrors = error.error;
    });
  }

  signUpWithTwitter() {
    const windowRef: Window = window.open(
                                '/accounts/twitter/login/?next=%2Fapi%2Fprofile%2Fclose',
                                'twitter-window',
                                'menubar=false,toolbar=false');

    const that = this;
    const popupTick = setInterval(function() {
      if (windowRef.closed) {
        clearInterval(popupTick);
        that.authService.getTokenFromServer().subscribe((data: any) => {
          AuthService.loginSuccess.emit(data);
        }, (error: any) => {
          this.socialErrors = 'Erro ao logar com o Twitter';
        });
      }
    }, 500);
  }

}
