import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { NgxCaptchaModule } from 'ngx-captcha';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { FormErrorsModule } from '../form-errors/form-errors.module';

import { LoginComponent } from './login/login.component';
import { ParagraphsPipe } from './pipes/paragraphs.pipe';
import { ProfileComponent } from './profile/profile.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ProfileTwitterConnectComponent } from './profile-connect/profile-connect.component';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';
import { RecoverPasswordConfirmComponent } from './recover-password-confirm/recover-password-confirm.component';
import { RegisterComponent } from './register/register.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';

import { AddressService } from '../services/address.service';
import { AuthService} from '../services/auth.service';
import { ProfileCategoriesService } from '../services/profile-categories.service';
import { ProfileService } from '../services/profile.service';
import { SocialFacebookService } from '../services/social-facebook.service';

import { environment } from '../../environments/environment';
import { Profile } from 'selenium-webdriver/firefox';
import { TextMaskModule } from 'angular2-text-mask';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    LoginComponent,
    ParagraphsPipe,
    ProfileComponent,
    ProfileEditComponent,
    RecoverPasswordComponent,
    RecoverPasswordConfirmComponent,
    RegisterComponent,
    VerifyEmailComponent,
    ProfileTwitterConnectComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    FormErrorsModule,
    RouterModule,
    TextMaskModule,
    NgxCaptchaModule.forRoot({
      invisibleCaptchaSiteKey: environment.invisibleCaptchaSiteKey
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    AddressService,
    AuthService,
    ProfileCategoriesService,
    ProfileService,
    SocialFacebookService,
  ],
  exports: [
    LoginComponent,
    ProfileComponent,
    ProfileEditComponent,
    RecoverPasswordComponent,
    RecoverPasswordConfirmComponent,
    RegisterComponent,
    VerifyEmailComponent,
  ]
})
export class UsersModule { }
