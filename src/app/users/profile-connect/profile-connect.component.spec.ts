import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileTwitterConnectComponent } from './profile-connect.component';

describe('ProfileConnectComponent', () => {
  let component: ProfileTwitterConnectComponent;
  let fixture: ComponentFixture<ProfileTwitterConnectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileTwitterConnectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileTwitterConnectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
