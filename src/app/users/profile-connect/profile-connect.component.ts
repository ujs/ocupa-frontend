import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute } from '@angular/router';
import { SocialTwitterService } from '../../services/social-twitter.service';

@Component({
  selector: 'app-profile-connect',
  templateUrl: './profile-connect.component.html',
  styleUrls: ['./profile-connect.component.scss']
})
export class ProfileTwitterConnectComponent implements OnInit {
  error: any;

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private socialTwitterService: SocialTwitterService,
  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      const oauth_token = params['oauth_token'];
      const oauth_verifier = params['oauth_verifier'];
      this.authService.requestTwitterAccessToken(oauth_token, oauth_verifier).subscribe(
        data => {
          this.authService.connectTwitter(data['oauth_token'][0], data['oauth_token_secret'][0]).subscribe(
            data => {
              close();
            },
            error => {console.log(error); this.error = error.error;}
          );
        },
        error => {console.log(error); this.error = error.error;}
      )
    });
  }
}
