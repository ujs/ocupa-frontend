import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { throttle } from 'lodash';

import { AuthService } from '../services/auth.service';
import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'app-discussion',
  templateUrl: './discussion.component.html',
  styleUrls: ['./discussion.component.scss']
})
export class DiscussionComponent implements AfterViewInit, OnInit {

  @ViewChild('iframe') iframe: ElementRef;

  constructor(private profileService: ProfileService, private router: Router) { }

  public resizeIframe() {
    if (this.iframe.nativeElement.contentWindow.document.body) {
      this.iframe.nativeElement.style.height = this.iframe.nativeElement.contentWindow.document.body.scrollHeight + 'px';
    }
  }

  ngOnInit() {
    if (!this.profileService.get()) {
      AuthService.loginSuccessNext = '/forum';
      this.router.navigate(['login']);
    }
  }

  ngAfterViewInit() {
    window.addEventListener('message', throttle(this.resizeIframe.bind(this), 100));
  }
}
