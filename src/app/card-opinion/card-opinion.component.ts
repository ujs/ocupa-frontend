import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'card-opinion',
  templateUrl: './card-opinion.component.html',
  styleUrls: ['./card-opinion.component.scss']
})
export class CardOpinionComponent implements OnInit {
  @Input() opinion = {
    cor: '',
    image: '',
    category: '',
  };

  constructor() { }

  ngOnInit() {
  }

}
