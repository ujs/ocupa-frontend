import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

import { LoadingBarService } from '@ngx-loading-bar/core';
import { TranslateService } from '@ngx-translate/core';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';

import { ProfileService } from './services/profile.service';
import { NotificationService } from './services/notification.service';
import { AuthService } from './services/auth.service';
import { Profile } from './models/profile';
import { NotificationInfo } from './models/notification-info';

import { environment } from '../environments/environment';

import * as Raven from 'raven-js';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app';
  profile: Profile;

  constructor(translate: TranslateService,
              public loader: LoadingBarService,
              angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics,
              private router: Router,
              private profileService: ProfileService,
              private notificationService: NotificationService) {
    translate.setDefaultLang('pt-br');
    translate.use('pt-br');
  }

  ngOnInit() {
    // Reset scroll on every route change
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }

      if (evt.url.indexOf('#') === -1) {
        window.scrollTo(0, 0);
      }
    });

    // Prepare user data to be sent to Sentry if needed
    this.profile = this.profileService.get();
    this.sentryUserData(this.profile);

    ProfileService.profileChange.subscribe(profile => {
      this.profile = profile;
      this.sentryUserData(profile);
    });

    // Register the OneSignal app, if app is not inside iframe
    if (!this.insideIframe) {
      this.startOneSignal(window['OneSignal'] || []);
    }
  }

  startOneSignal(OneSignal) {
    OneSignal.push(['init', {
      appId: environment.onSignalAppId,
      autoRegister: true,
      allowLocalhostAsSecureOrigin: true,
      notifyButton: {
        enable: false
      }
    }]);

    OneSignal.push(['registerForPushNotifications']);

    const that = this;

    OneSignal.push(function () {
      // Occurs when the user's subscription changes to a new value.
      OneSignal.on('subscriptionChange', function (isSubscribed) {
        console.log('The user\'s subscription state is now:', isSubscribed);
        if (isSubscribed) {
          OneSignal.getUserId().then(function (userId) {
            console.log('User ID is', userId);

            // Save the user ID in local storage for future reference
            const notificationInfo = new NotificationInfo();
            notificationInfo.oneSignalAppId = userId;
            that.notificationService.saveInfo(notificationInfo);

            // Save the user email in OneSignal, if it's available
            if (that.profile) {
              that.notificationService.sendEmail(that.profile.email);
              that.notificationService.sendOneSignalId(userId);
            }
          });
        }
      });
    });
  }

  sentryUserData(profile) {
    // Prepare user data to be sent to Sentry in case of an error
    if (profile) {
      Raven.setUserContext({
        id: profile.id,
        username: profile.username,
        email: profile.email
      });
    } else {
      Raven.setUserContext();
    }
  }

  public get insideIframe(): boolean {
    try {
      return window.self !== window.top;
    } catch {
      return true; // Probably CORS problem.
    }
  }

}
