import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { StaticPage } from '../models/static-page';
import { StaticPagesService } from '../services/static-page.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  public page: StaticPage;

  constructor(
    private pagesService: StaticPagesService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.route.url.subscribe(fragments => {
      const path = fragments.map(fragment => fragment.path).join('/');
      this.pagesService.get(path).subscribe(pages => {
        if (pages && pages.length > 0) {
          this.page = pages[0];
        } else {
          this.router.navigate(['404']);
        }
      }, error => {
        this.router.navigate(['404']);
      });
    });
  }

}
