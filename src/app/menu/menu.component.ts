import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../services/profile.service';
import { AuthService } from '../services/auth.service';

import { Profile } from '../models/profile';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  menuActive = false;
  profile: Profile;

  constructor(private profileService: ProfileService,
              private authService: AuthService,
  ) { }

  ngOnInit() {
    // Load the user profile, if available
    this.profile = this.profileService.get();

    ProfileService.profileChange.subscribe(profile => {
      this.profile = profile;
    }, error => {});
  }

  logout() {
    AuthService.logout.emit();
  }

}
