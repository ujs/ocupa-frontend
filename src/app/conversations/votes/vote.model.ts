export enum VoteAction {
    AGREE = 1,
    PASS = 0,
    DISAGREE = -1,
}

export class Vote {
    id: number;
    comment: number;
    value: VoteAction;
    created_at: string;
}
