import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { Comment } from '../comments/comment.model';
import { Vote, VoteAction } from './vote.model';

@Injectable()
export class VoteService {

  constructor(private http: HttpClient) { }

  agree(comment: Comment): Observable<Vote> {
    return this.vote(comment, VoteAction.AGREE);
  }

  disagree(comment: Comment): Observable<Vote> {
    return this.vote(comment, VoteAction.DISAGREE);
  }

  pass(comment: Comment): Observable<Vote> {
    return this.vote(comment, VoteAction.PASS);
  }

  public vote(comment: Comment, action: VoteAction): Observable<Vote> {
    const vote = new Vote();
    vote.comment = comment.id;
    vote.value = action;

    // Send the vote to the pushTogether backend
    return this.save(vote);
  }

  save(vote: Vote): Observable<Vote> {
    const fullEndpointUrl = `${environment.apiUrl}/api/votes/`;
    return this.http.post<Vote>(fullEndpointUrl, vote);
  }
}
