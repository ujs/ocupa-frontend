import { Comment } from '../comments/comment.model';
import { CategoryCustomization } from '../category/category-customization';

export class Conversation {

    public title: string;
    public slug: string;
    public description: string;
    public approved_comments: number;
    public total_approved_comments: number;
    public total_votes: number;
    public response: string;
    public polis_url : string;
    public dialog: string;
    public opinion: string;
    public created_at: string;
    public updated_at: string;
    public user_participation_ratio: number;
    public background_image: string;
    public background_color: string;
    public id: number;
    public author: any;
    public position: number;
    public is_new: boolean;
    public category_id: number;
    public category_name: string;
    public category_slug: string;
    public category_customizations: CategoryCustomization;

}
