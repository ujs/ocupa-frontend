import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ConversationService } from './conversation.service';
import { VoteService } from '../votes/votes.service';
import { CommentService } from '../comments/comments.service';
import { ProfileService } from '../../services/profile.service';
import { AuthService } from '../../services/auth.service';
import { Conversation } from './conversation.model';
import { Comment } from '../comments/comment.model';
import { Vote, VoteAction } from '../votes/vote.model';
import { Nudge } from '../nudge/nudge-model';
import { environment } from '../../../environments/environment';

import { YoutubePlayerModule } from 'ngx-youtube-player';

import * as _ from 'lodash';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.scss'],
  providers: [ConversationService, CommentService],
})
export class ConversationComponent implements OnInit {

  @Input() conversation: Conversation;
  currentComment: Comment;
  newCommentText: string;
  newCommentStatus: string;
  player: any;
  videoModalActive = false;
  errorModalActive = false;

  nudge : Nudge;
  VoteAction = VoteAction;

  constructor(private conversationService: ConversationService,
              private voteService: VoteService,
              private commentService: CommentService,
              private profileService: ProfileService,
              private route: ActivatedRoute,
              private router: Router,
              private http: HttpClient,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.conversationService.get(params.slug).subscribe(conversation => {
        this.conversation = conversation;

        // Get a random comment
        this.conversationService.getNextUnvotedComment(conversation.id).subscribe(
          data => this.currentComment = data,
          error => {}
        );
      }, error => {});
    }, error => {});
  }

  controlVideo(action) {
    this.videoModalActive = action === 'play';
  }

  savePlayer(player) {
    this.player = player;

    this.player[(this.videoModalActive ? 'play' : 'pause') + 'Video']();
  }

  onStateChange(event) {
    if (event.data === 0) {
      this.videoModalActive = false;
    }
  }

  vote(comment, action: VoteAction) {
    if (!this.profileService.get()) {
      AuthService.loginSuccessNext = `/conversas/${this.conversation.slug}`;
      this.router.navigate(['login']);
    } else {
      this.voteService.vote(comment, action).subscribe(voteData => {
        this.conversationService.getNextUnvotedComment(this.conversation.id).subscribe(anothercomment => {
          this.currentComment = anothercomment;
        }, error => {
          this.currentComment = null;
        });
      }, error => {});
    }
  }

  sendComment() {
    if (!this.profileService.get()) {
      AuthService.loginSuccessNext = `/conversas/${this.conversation.slug}`;
      this.router.navigate(['login']);
      return;
    }

    const newcomment = new Comment();
    newcomment.content = this.newCommentText;
    newcomment.conversation = this.conversation.id;

    this.commentService.create(newcomment).subscribe(response => {
      this.newCommentText = '';
      this.newCommentStatus = 'success';
    }, (response) => {
      this.nudge = response.error.nudge;
      this.newCommentStatus = 'failed';
      this.errorModalActive = true;
    });
  }

  commentCharCounter(str) {
    this.newCommentText = str;

    if (str.length > 280) {
      this.newCommentText = this.newCommentText.substr(0, 280);
    }
  }


  ratio(conversation: Conversation) {
    // if(!conversation)
    //   return 0;
    let ratio = conversation.user_participation_ratio;
    if (!ratio) {
      ratio = 0;
    }
    ratio = 50;
    return ratio;
  }

  parserDate(strDate: string) {
    strDate = this.convertDate(strDate);
    if (_.isUndefined(strDate)) {
      return undefined;
    }

    const newDate = new Date(strDate);
    return newDate;
  }

  convertDate(date) {
    if (_.isUndefined(date)) {
      return undefined;
    }

    const dateArray = date.split('-');
    const newDate = dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0];

    return newDate;
  }

}
