import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';
import * as _ from 'lodash';

import { Conversation } from '../conversation/conversation.model';

@Component({
  selector: 'app-conversations',
  templateUrl: './conversations.component.html',
  styleUrls: ['./conversations.component.scss']
})
export class ConversationsComponent implements OnChanges {

  @Input() conversations: Conversation[];
  @Input() filter: string = null;
  @Output() onPlayVideo = new EventEmitter<Conversation>();

  categorizedConversations: Conversation[];
  categories: any = {};
  categoryNames: string[];

  constructor() { }

  ngOnChanges() {
    const categorizedConversations = [];
    const categories = [];
    const categoryNames: string[] = [''];

    if (!this.hasConversation(this.conversations)) {
      this.conversations = [];
    }
    this.conversations.forEach(conversation => {
      if (conversation.category_name) {
        if (categoryNames.indexOf(conversation.category_name) === -1) {
          categoryNames.push(conversation.category_name);
          categories[conversation.category_name] = {
            styles: conversation.category_customizations.styles,
            slug: conversation.category_slug
          };
          if (!categorizedConversations[conversation.category_name]) {
            categorizedConversations[conversation.category_name] = [];
          }
          categorizedConversations[conversation.category_name].push(conversation);
        }
      }
    });
    this.conversations = _.sortBy(this.conversations, ['position']);
    this.categorizedConversations = this.conversations.map(conversations => _.sortBy(conversations, ['position']));
    this.categories = categories;
    this.categoryNames = categoryNames;
  }

  get filteredConversations(): Conversation[] {
    if (this.categorizedConversations) {
      if (this.filter) {
        return this.categorizedConversations[this.filter];
      } else {
        return this.conversations;
      }
    } else {
      return [];
    }
  }

  hasConversation(conversations: Conversation[]) {
    let hasConversation = _.isNil(conversations);
    hasConversation = hasConversation ? false : !_.isEmpty(conversations);
    return hasConversation;
  }

  playVideo(conversation: Conversation) {
    this.onPlayVideo.emit(conversation);
  }

}
