import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConversationComponent } from './conversation/conversation.component';
import { ConversationListComponent } from './conversation-list/conversation-list.component';

const routes: Routes = [
  {
    path: 'conversas',
    children: [
      { path: '', component: ConversationListComponent },
      { path: ':slug', component: ConversationComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConversationsRoutingModule { }
