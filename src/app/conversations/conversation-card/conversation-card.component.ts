import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

import { Conversation } from '../conversation/conversation.model';

@Component({
  selector: 'conversation-card',
  templateUrl: './conversation-card.component.html',
  styleUrls: ['./conversation-card.component.scss']
})
export class ConversationCardComponent implements OnInit {
  @Input() conversation: Conversation;
  @Output() onPlayVideo: EventEmitter<Boolean> = new EventEmitter<Boolean>();

  public playing = false;
  public bgImage = '';

  constructor() { }

  ngOnInit() {
    this.bgImage = 'url(' + (
      this.conversation.background_image ?
        this.conversation.background_image :
        (this.conversation.polis_url ? 'https://img.youtube.com/vi/' + this.conversation.polis_url + '/mqdefault.jpg' : '')) + ')';
  }

  playVideo() {
    this.playing = true;
    this.onPlayVideo.emit(this.playing);
  }

}
