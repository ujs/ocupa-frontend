import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ConversationsRoutingModule } from './conversations-routing.module';
import { ConversationComponent } from './conversation/conversation.component';
import { ConversationsComponent } from './conversations/conversations.component';
import { CommentsComponent } from './comments/comments.component';
import { VotesComponent } from './votes/votes.component';
import { ConversationListComponent } from './conversation-list/conversation-list.component';
import { MenuModule } from '../menu/menu.module';
import { PageHeaderModule } from '../page-header/page-header.module';
import { ConversationCardComponent } from './conversation-card/conversation-card.component';
import { NudgeComponent } from './nudge/nudge.component';

import { ConversationService } from './conversation/conversation.service';
import { VoteService } from './votes/votes.service';

import { YoutubePlayerModule } from 'ngx-youtube-player';



@NgModule({
  imports: [
    CommonModule,
    ConversationsRoutingModule,
    FormsModule,
    YoutubePlayerModule,
    MenuModule,
    PageHeaderModule,
  ],
  declarations: [
    ConversationComponent,
    ConversationsComponent,
    CommentsComponent,
    VotesComponent,
    ConversationListComponent,
    ConversationCardComponent,
    NudgeComponent,
  ],
  providers: [
    VoteService
  ],
  exports: [
    ConversationsComponent
  ]
})
export class ConversationsModule { }
