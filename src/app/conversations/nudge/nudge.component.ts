import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { Nudge } from './nudge-model';

@Component({
  selector: 'nudge',
  templateUrl: './nudge.component.html',
  styleUrls: ['./nudge.component.scss']
})
export class NudgeComponent implements OnInit {
  @Input('nudge') nudge: Nudge
  @Output() closeNudge = new EventEmitter()

  constructor() {
  }

  ngOnInit() {
    this.nudge.imagePath = '/assets/images/nudges/' + this.nudge.state + '.svg'
  }

  close(){
    this.closeNudge.emit(true);
  }
}
