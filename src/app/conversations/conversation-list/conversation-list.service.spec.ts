import { TestBed, inject } from '@angular/core/testing';

import { ConversationListService } from './conversation-list.service';

describe('ConversationListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ConversationListService]
    });
  });

  it('should be created', inject([ConversationListService], (service: ConversationListService) => {
    expect(service).toBeTruthy();
  }));
});
