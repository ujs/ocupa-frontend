import { Component, OnInit } from '@angular/core';

import { Conversation } from '../conversation/conversation.model';
import { ConversationService } from '../conversation/conversation.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-conversation-list',
  templateUrl: './conversation-list.component.html',
  styleUrls: ['./conversation-list.component.scss'],
  providers: [ConversationService],
})
export class ConversationListComponent implements OnInit {

  conversations: Conversation[];

  videoModalConversation: any;
  videoModalActive = false;
  player;

  constructor(private conversationService: ConversationService) { }

  ngOnInit() {
    this.conversationService.list().subscribe((conversations: Conversation[]) => {
      this.conversations = conversations;
    }, error => {
      console.log(error);
    });
  }

  backgroundImage(conversation: Conversation): string {
    const imagem_path = (_.isNil(conversation.background_image)) ? '/assets/theme/card-bg.jpg' : conversation.background_image;
    return imagem_path;
  }

  controlVideo(action, conversation) {
    this.videoModalActive = action === 'play';
    this.videoModalConversation = conversation;
  }

  savePlayer(player) {
    this.player = player;

    this.player[(this.videoModalActive ? 'play' : 'pause') + 'Video']();
  }

  onStateChange(event) {
    if (event.data === 0) {
      this.videoModalActive = false;
    }
  }
}
