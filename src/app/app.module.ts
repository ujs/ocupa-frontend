import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';

import { HttpModule } from '@angular/http';
import { LOCALE_ID } from '@angular/core';
import localePtBr from '@angular/common/locales/pt';

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { LoaderComponent } from './loader/loader.component';
import { ProfilePageComponent } from './profile/profile.component';
import { ProfileEditPageComponent } from './profile-edit/profile-edit.component';
import { DoacaoComponent } from './doacao/doacao.component';
import { RegisterPageComponent } from './register/register.component';
import { LoginPageComponent } from './login/login.component';
import { RecoverPasswordPageComponent } from './recover-password/recover-password.component';
import { RecoverPasswordConfirmPageComponent } from './recover-password-confirm/recover-password-confirm.component';
import { VerifyEmailPageComponent } from './verify-email/verify-email.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SubhomeManuComponent } from './subhome-manu/subhome-manu.component';
import { UJSAffiliationFacadeComponent } from './ujs/affiliation-facade/ujs-affiliation-facade.component';
import { UJSAffiliationFormComponent } from './ujs/affiliation-form/ujs-affiliation-form.component';
import { UJSLoginComponent } from './ujs/login/ujs-login.component';
import { UJSNewAffiliationComponent } from './ujs/new-affiliation/ujs-new-affiliation.component';
import { UJSPostAffiliationComponent } from './ujs/post-affiliation/ujs-post-affiliation.component';

import { ReceiptsComponent } from './receipts/receipts.component';
import { MessagesComponent } from './messages/messages.component';
import { CardOpinionComponent } from './card-opinion/card-opinion.component';
import { CardPostComponent } from './card-post/card-post.component';
import { PageHeaderModule } from './page-header/page-header.module';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeFooterComponent } from './home-footer/home-footer.component';
import { PageComponent } from './page/page.component';

import { AuthService } from './services/auth.service';
import { ProfileService } from './services/profile.service';
import { DonationService } from './services/donation.service';
import { UJSAffiliationService } from './services/ujs-affiliation.service';

import { rootRouterConfig } from './app.routes';
import { HttpsRequestInterceptor } from './interceptor.module';
import { environment } from '../environments/environment';

// angulartics2
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';
import { YoutubePlayerModule } from 'ngx-youtube-player';

import { TextMaskModule } from 'angular2-text-mask';

import { CookieService } from 'ngx-cookie-service';

// ngx-facebook
import { FacebookModule } from 'ngx-facebook';

import { Ng2Webstorage } from 'ngx-webstorage';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxCaptchaModule } from 'ngx-captcha';

import { SocialFacebookService } from './services/social-facebook.service';
import { SocialTwitterService } from './services/social-twitter.service';
import { AddressService } from './services/address.service';
import { CPFValidationService } from './services/cpf-validation.service';
import { ProfileCategoriesService } from './services/profile-categories.service';
import { OpinionService } from './services/opinion.service';
import { StaticPagesService } from './services/static-page.service';
import { NotificationService } from './services/notification.service';

import { CampaignModule } from './campaign/campaign.module';
import { ConversationsModule } from './conversations/conversations.module';
import { FormErrorsModule } from './form-errors/form-errors.module';
import { MenuModule } from './menu/menu.module';
import { UsersModule } from './users/users.module';

// Add operators project wide
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';

registerLocaleData(localePtBr, 'pt-BR');

import * as Raven from 'raven-js';
import { ParagraphsPipe } from './pipes/paragraphs.pipe';
import { DiscussionComponent } from './discussion/discussion.component';
import { CandidatesComponent } from './candidates/candidates.component';
import { CandidateCardComponent } from './candidate-card/candidate-card.component';

if (environment.sentryDSN) {
  Raven.config(environment.sentryDSN)
       .install();
}

export class RavenErrorHandler implements ErrorHandler {
  handleError(err: any): void {
    console.error(err);
    // If an HttpErrorResponse has been thrown, an event has already been sent to Sentry
    // Therefore, the usual error handling must be bypassed on that case
    if (!(err instanceof HttpErrorResponse)) {
      Raven.captureException(err.originalError || err);
    }
  }
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DoacaoComponent,
    LoaderComponent,
    ProfilePageComponent,
    ProfileEditPageComponent,
    ReceiptsComponent,
    MessagesComponent,
    RegisterPageComponent,
    LoginPageComponent,
    RecoverPasswordPageComponent,
    DashboardComponent,
    CardOpinionComponent,
    CardPostComponent,
    NotFoundComponent,
    SubhomeManuComponent,
    HomeFooterComponent,
    PageComponent,
    CandidatesComponent,
    CandidateCardComponent,
    ParagraphsPipe,
    RecoverPasswordConfirmPageComponent,
    UJSAffiliationFacadeComponent,
    UJSAffiliationFormComponent,
    UJSLoginComponent,
    UJSNewAffiliationComponent,
    UJSPostAffiliationComponent,
    VerifyEmailPageComponent,
    DiscussionComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    FormErrorsModule,
    UsersModule,
    CampaignModule,
    LoadingBarHttpClientModule,
    LoadingBarModule.forRoot(),
    TextMaskModule,
    FacebookModule.forRoot(),
    NgbModule.forRoot(),
    NgxCaptchaModule.forRoot({
      invisibleCaptchaSiteKey: environment.invisibleCaptchaSiteKey
    }),
    YoutubePlayerModule,
    MenuModule,
    PageHeaderModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    Ng2Webstorage.forRoot({ prefix: 'ujs-ocupa', caseSensitive: true }),
    Angulartics2Module.forRoot([Angulartics2GoogleAnalytics]),
    ConversationsModule,
    RouterModule.forRoot(rootRouterConfig, { useHash: false }),
  ],
  providers: [
    AuthService,
    CookieService,
    ProfileService,
    SocialFacebookService,
    SocialTwitterService,
    DonationService,
    AddressService,
    CPFValidationService,
    OpinionService,
    ProfileCategoriesService,
    StaticPagesService,
    NotificationService,
    UJSAffiliationService,
    { provide: ErrorHandler, useClass: RavenErrorHandler },
    { provide: LOCALE_ID, useValue: 'pt-BR' },
    { provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true },
  ],
  entryComponents: [
    LoginPageComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
