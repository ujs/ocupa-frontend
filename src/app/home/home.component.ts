import { ChangeDetectorRef, Component, OnInit, Input, ViewChild } from '@angular/core';

import { InvisibleReCaptchaComponent } from 'ngx-captcha';
import { YoutubePlayerModule } from 'ngx-youtube-player';

import { DomSanitizer, SafeResourceUrl, } from '@angular/platform-browser';
import { AddressService } from '../services/address.service';
import { AuthService } from '../services/auth.service';
import { City, Region } from '../models/address';
import { NgForm } from '@angular/forms';
import { CandidateService } from '../services/candidate.service';
import { Candidate } from '../models/candidate';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [CandidateService]
})
export class HomeComponent implements OnInit {
  user: any = {};
  @ViewChild('join_us') joinUsForm: NgForm;
  @ViewChild('recaptcha') recaptcha: InvisibleReCaptchaComponent;
  public cepMask = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];

  captchaResponse?: string;
  captchaWaiting = false;
  captchaInvalid = false;

  videoModalActive = false;
  videoSrc: SafeResourceUrl;
  player: any;

  regions: Region[];
  cities: City[];

  candidates: Array<Candidate>;
  slideIndex = 0;

  constructor(
    private addressService: AddressService,
    private authService: AuthService,
    private candidateService: CandidateService,
    private changeDetector: ChangeDetectorRef,
    public sanitizer: DomSanitizer,
  ) { }

  ngOnInit() {
    this.candidateService.getCandidates().subscribe( (response: Array<Candidate> ) => this.candidates = response, error => {} );
  }

  controlVideo(action) {
    this.videoModalActive = action === 'play';
  }

  savePlayer(player) {
    this.player = player;

    this.player[(this.videoModalActive ? 'play' : 'pause') + 'Video']();
  }

  onStateChange(event) {
    if (event.data === 0) {
      this.videoModalActive = false;
    }
  }

  fetchAddress() {
    if (!this.user || !this.user.zipcode) {
      return;
    }
    if (!this.regions) {
      this.addressService.getRegions().subscribe(regions => {
        this.regions = regions;
      }, error => {});
    }

    const zipCode = this.user.zipcode.replace('-', '');
    this.addressService
      .fetchAddress(zipCode)
      .subscribe(data => {
        const state_uf = data.uf;
        const state = this.regions.filter((region) => {
          return region.alternate_names.indexOf(state_uf) >= 0;
        });
        if (state.length > 0) {
          this.user.state = state[0].id;
          this.addressService
            .getCitiesByRegion(this.user.state)
            .subscribe(cities => {
              this.cities = cities;
              const city = this.cities.filter((_city) => {
                return _city.name.indexOf(data.localidade) >= 0;
              });
              if (city.length > 0) {
                this.user.city = city[0].id;
              }
            }, error => {});
        }
      }, error => {});
  }

  joinUs() {
    if (this.joinUsForm.invalid) {
      this.joinUsForm.controls.name.markAsDirty();
      this.joinUsForm.controls.email.markAsDirty();
      this.joinUsForm.controls.zipcode.markAsDirty();
      return false;
    }
    this.user.origin = 'ocupa';
    this.authService
      .saveUser({ 'user': this.user, 'token': this.captchaResponse })
      .subscribe((data: any) => {
        if (!data.error) {
            this.user.subscribed = true;
        } else {
            if (data.message === 'invalid captcha') {
              this.captchaInvalid = true;
            } else {
              this.user.exists = true;
            }
        }
        this.changeDetector.detectChanges();
      }, error => {});
  }

  markAsHuman(response: string) {
    this.captchaResponse = response;
    this.captchaWaiting = false;
    this.joinUs();
  }

  verifyRobot() {
    this.recaptcha.execute();
    this.captchaWaiting = true;
  }

  nextSlide() {
    console.log(this.slideIndex, this.candidates.length - 1);

    if (this.slideIndex < this.candidates.length - 1) {
      this.slideIndex++;
    }
  }

  prevSlide() {
    if (this.slideIndex - 1 >= 0 ) {
      this.slideIndex--;
    }
  }
}
