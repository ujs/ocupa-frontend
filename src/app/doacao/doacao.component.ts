import { AfterViewInit, Component, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';

import { Address } from '../models/address';
import { Profile } from '../models/profile';
import {
  PagSeguroCheckoutSession,
  Item,
  Payment,
  PaymentMethod,
  PaymentType
} from '../models/checkout';
import { DonationService } from '../services/donation.service';
import { ProfileService } from '../services/profile.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { AddressService } from '../services/address.service';
import { Validators, NgForm } from '@angular/forms';

declare var PagSeguroDirectPayment: any;
@Component({
  selector: 'app-doacao',
  templateUrl: './doacao.component.html',
  styleUrls: ['./doacao.component.scss']
})
export class DoacaoComponent implements OnInit {

  public static cardOptionsAcquired: EventEmitter<any> = new EventEmitter();
  public static cardBrandAcquired: EventEmitter<any> = new EventEmitter();
  public static bankOptionsAcquired: EventEmitter<any> = new EventEmitter();
  public static cardTokenCreated: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('donation_form') form: NgForm;

  public bank_options: Array<any>;
  public card_options: Array<any>;
  public regions: Array<any>;
  public cities: Array<any>;
  public valores = [
    25,
    50,
    100,
    500,
    1000,
    2000
  ];
  public alternative_value: number;
  public agreement = false;
  public user_is_card_holder = false;
  public step = 1;

  public user: Profile;
  public checkout: PagSeguroCheckoutSession;
  public method = PaymentMethod;

  public form_errors = <any>{};

  public phoneMask = ['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public cpfMask = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  public dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public cepMask = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
  public expirationDateMask = [/\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];

  constructor(private donationService: DonationService,
    private authService: AuthService,
    private router: Router,
    private profileService: ProfileService,
    private addressService: AddressService) {

    this.checkout = new PagSeguroCheckoutSession(new Profile(), 'UJSDONATION');
    this.checkout.payment = new Payment();
    this.checkout.items.push(new Item('Doação', 'Doação', this.valores[0], 1));
    this.checkout.method = this.method.Credito;

    this.addressService.getRegions().subscribe((regions: any) => {
      this.regions = regions;
    }, error => {});

    this.donationService.getTokenFromServer().subscribe(res => {
      this.checkout.session_id = res.session;
      PagSeguroDirectPayment.setSessionId(this.checkout.session_id);

      PagSeguroDirectPayment.getPaymentMethods({
        amount: this.checkout.calculateAmount(),
        success: function (data) {
          if (data.paymentMethods.CREDIT_CARD) {
            DoacaoComponent.cardOptionsAcquired
              .emit(data.paymentMethods.CREDIT_CARD.options);
          }
          if (data.paymentMethods.ONLINE_DEBIT) {
            DoacaoComponent.bankOptionsAcquired
              .emit(data.paymentMethods.ONLINE_DEBIT.options);
          }
        },
        error: function (error) {
          DoacaoComponent.cardOptionsAcquired.error(error);
        },
      });
    }, error => {
      // this.step = 0;
    });

    DoacaoComponent.cardTokenCreated.subscribe(token => {
      this.checkout.card_token = token.card.token;
      this.checkout.payment.card_cvv = undefined;
      this.checkout.payment.card_number = undefined;
      this.checkout.payment.brand = undefined;
      this.donationService.sendTransaction(this.checkout).subscribe(data => {
        this.checkout.status = data;
        this.handleDonationSuccess();
      }, error => {
        console.log(error);
      });
    }, error => {});

    DoacaoComponent.cardBrandAcquired.subscribe(brand => {
      this.checkout.payment.brand = brand.name;
      this.checkout.sender_hash = PagSeguroDirectPayment.getSenderHash();
      this.sendCreditCardTransaction();
    }, error => {
      console.log(error);
    });

    DoacaoComponent.cardOptionsAcquired.subscribe(cards => {
      this.card_options = Object.values(cards);
    }, error => {});

    DoacaoComponent.bankOptionsAcquired.subscribe(banks => {
      this.bank_options = Object.values(banks);
    }, error => {});

  }

  ngOnInit() {
  }

  authenticationStep() {
    if (!this.termsValidation()) {
      return;
    }
    this.step = 2;
  }

  paymentStep() {
    if (!this.authenticationValidation()) {
      return;
    }
    if (this.profileService.get() &&
      this.checkout.sender.email === this.profileService.get().email) {
      this.profileService.updateFromServer().subscribe(data => {
        this.checkout.sender = <Profile>Object
          .assign({}, this.profileService.get());
        if (this.checkout.sender.birth_date && this.checkout.sender.birth_date.includes('-')) {
          this.checkout.sender.birth_date = new DatePipe('en-US').transform(this.checkout.sender.birth_date, 'dd/MM/yyyy');
        }
        this.step = 3;
      }, error => {
        this.router.navigate(['login']);
      });
    } else {
      this.authService
        .getUserStatus(this.checkout.sender.email)
        .subscribe(res => {
          if (res.active) {
            this.router.navigate(['login']);
          } else {
            this.step = 3;
          }
        }, error => {
        });
    }
  }

  donationDataValidation(): Boolean {
    let errorFound: Boolean = false;
    this.form_errors = {};

    if (this.form.controls.holder_name.errors) {
      errorFound = true;
      this.form_errors['holder_name'] = {};
      if (this.form.controls.holder_name.errors.pattern) {
        this.form_errors['holder_name']['isInvalid'] =
          'Nome inválido, digite seu nome completo';
      }
    }
    if (this.form.controls.phone_number.errors) {
      errorFound = true;
      this.form_errors['phone_number'] = {};
      if (this.form.controls.phone_number.errors.pattern) {
        this.form_errors['phone_number']['isInvalid'] = 'Telefone inválido';
      }
    }
    if (this.form.controls.card_number.errors) {
      errorFound = true;
      this.form_errors['card_number'] = {};
    }
    if (this.form.controls.expiration_date.errors) {
      errorFound = true;
      this.form_errors['expiration_date'] = {};
      if (this.form.controls.expiration_date.errors.pattern) {
        this.form_errors['expiration_date']['isInvalid'] =
          'Data incompleta (formato: MM/AAAA)';
      }
    }
    if (this.form.controls.card_cvv.errors) {
      errorFound = true;
      this.form_errors['card_cvv'] = {};
      if (this.form.controls.card_cvv.errors.minlength) {
        this.form_errors['card_cvv']['inputLength'] = 'CVV incompleto';
      }
    }
    if (this.form.controls.holder_cpf.errors) {
      errorFound = true;
      this.form_errors['holder_cpf'] = {};
      if (this.form.controls.holder_cpf.errors.pattern) {
        this.form_errors['holder_cpf']['inputLength'] =
          'CPF do Titular inválido';
      }
    }
    if (this.form.controls.holder_birth_date.errors) {
      errorFound = true;
      this.form_errors['holder_birth_date'] = {};
      if (this.form.controls.holder_birth_date.errors.pattern) {
        this.form_errors['holder_birth_date']['isInvalid'] =
          'Data de Nascimento inválida';
      }
    }
    if (this.checkout.method !== PaymentMethod.Credito || !this.user_is_card_holder) {
      if (this.form.controls.name.errors) {
        errorFound = true;
        this.form_errors['name'] = {};
      }
      if (this.form.controls.birth_date.errors) {
        errorFound = true;
        this.form_errors['birth_date'] = {};
        if (this.form.controls.birth_date.errors.pattern) {
          this.form_errors['birth_date']['isInvalid'] =
            'Data de Nascimento inválida';
        }
      }
      if (this.form.controls.cpf.errors) {
        errorFound = true;
        this.form_errors['cpf'] = {};
        if (this.form.controls.cpf.errors.pattern) {
          this.form_errors['cpf']['inputLength'] = 'CPF do Doador inválido';
        }
      }
    }
    if (this.form.controls.postal_code.errors) {
      errorFound = true;
      this.form_errors['postal_code'] = {};
      if (this.form.controls.postal_code.errors.pattern) {
        this.form_errors['postal_code']['isInvalid'] = 'CEP inválido';
      }
    }
    if (this.form.controls.state.errors) {
      errorFound = true;
      this.form_errors['state'] = {};
    }
    if (this.form.controls.district.errors) {
      errorFound = true;
      this.form_errors['district'] = {};
    }
    if (this.form.controls.city.errors) {
      errorFound = true;
      this.form_errors['city'] = {};
    }
    if (this.form.controls.street.errors) {
      errorFound = true;
      this.form_errors['street'] = {};
    }
    if (this.form.controls.number.errors) {
      errorFound = true;
      this.form_errors['number'] = {};
    }
    if (this.form.controls.complement.errors) {
      errorFound = true;
      this.form_errors['complement'] = {};
    }

    return !errorFound;
  }

  authenticationValidation(): Boolean {
    let errorFound: Boolean = false;
    this.form_errors = {};

    if (this.form.controls.email.invalid) {
      errorFound = true;
      this.form_errors['email'] = {};
      if (this.form.controls.email.errors.required) {
        this.form_errors['email']['isEmpty'] = 'Campo de e-mail é obrigatório';
      } else if (this.form.controls.email.errors.email) {
        this.form_errors['email']['isInvalid'] = 'E-mail inválido';
      }
    }

    return !errorFound;
  }

  termsValidation(): Boolean {
    let errorFound: Boolean = false;
    this.form_errors = {};

    const valor_radio = this.checkout.calculateAmount();
    if ((!this.isNumber(this.amount) ||
      parseFloat(this.amount.toString()) <= 0)) {
      errorFound = true;
      this.form_errors['value'] = {};
      this.form_errors['value']['isInvalid'] =
        'É necessário selecionar um valor de doação válido';
    }

    if (this.agreement === undefined || this.agreement === false) {
      errorFound = true;
      this.form_errors['agreement'] = {};
      this.form_errors['agreement']['isEmpty'] =
        'É necessário aceitar os Termos de Uso para continuar';
    }

    return !errorFound;
  }

  sendPayment() {
    if (this.checkout.method === PaymentMethod.Credito && this.user_is_card_holder) {
      this.checkout.sender.name = this.checkout.payment.holder_name;
      this.checkout.sender.birth_date = this.checkout.payment.holder_birth_date;
      this.checkout.sender.cpf = this.checkout.payment.holder_cpf;
    }
    if (!this.donationDataValidation()) {
      return false;
    }
    if (this.checkout.method === PaymentMethod.Credito) {
      PagSeguroDirectPayment.getBrand({
        cardBin: this.checkout.payment.card_number,

        success: function (data) {
          DoacaoComponent.cardBrandAcquired.emit(data.brand);
        },
        error: function (error) {
          console.log(error);
        },
      });
    } else if (this.checkout.method === PaymentMethod.Boleto) {
      this.donationService.sendTransaction(this.checkout).subscribe(data => {
        // TODO handle Boleto
        console.log(data);
      }, error => {
        console.log(error);
      });
    }
  }

  sendCreditCardTransaction() {
    PagSeguroDirectPayment.createCardToken({
      cardNumber: this.checkout.payment.card_number,
      brand: this.checkout.payment.brand,
      cvv: this.checkout.payment.card_cvv,
      expirationMonth: this.checkout.payment.expiration_date.split('/')[0],
      expirationYear: this.checkout.payment.expiration_date.split('/')[1],
      success: function (data) {
        DoacaoComponent.cardTokenCreated.emit(data);
      },
      error: function (error) {
        console.log(error);
      },
    });
  }

  fetchAddress() {
    this.form_errors = {};
    if (this.form.controls.postal_code.errors) {
      this.form_errors['postal_code'] = {};
      if (this.form.controls.postal_code.errors.pattern) {
        this.form_errors['postal_code']['isInvalid'] = 'CEP inválido';
      }
      return;
    }
    if (!(this.checkout && this.checkout.payment && this.checkout.payment.postal_code)) {
      return;
    }

    const zipCode = this.checkout.payment.postal_code.replace('-', '');
    this.addressService
      .fetchAddress(zipCode)
      .subscribe((data: Address) => {
        const state_uf = data.uf;
        this.checkout.payment.state_uf = state_uf;
        const state = this.regions.filter((region) => {
          return region.alternate_names.indexOf(state_uf) >= 0;
        });
        if (state.length > 0) {
          this.checkout.payment.state = state[0].id;
          this.addressService
            .getCitiesByRegion(this.checkout.payment.state)
            .subscribe(cities => {
              this.cities = cities;
              const city = this.cities.filter((_city) => {
                return _city.name.indexOf(data.localidade) >= 0;
              });
              if (city.length > 0) {
                this.checkout.payment.city = city[0].id;
              }
            }, error => {});
        }
        this.checkout.payment.district = data.bairro;
        this.checkout.payment.street = data.logradouro;
      }, error => {});
  }

  getCities() {
    this.addressService
      .getCitiesByRegion(this.checkout.payment.state)
      .subscribe(data => {
        this.cities = data;
      }, error => {});
  }

  private isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  get amount() {
    if (this.alternative_value) {
      return this.alternative_value;
    }
    return this.checkout.calculateAmount();
  }

  get city() {
    if (this.cities && this.checkout.payment.city) {
      return this.cities.filter(city => {
        return city.id === this.checkout.payment.city;
      })[0].name;
    }
    return '';
  }

  get state() {
    if (this.checkout.payment.state_uf) {
      return this.checkout.payment.state_uf;
    }
    return '';
  }

  objectEmpty(obj) {
    if (!obj) {
      return false;
    }
    return Object.keys(obj).length === 0;
  }

  private handleDonationSuccess() {
    this.form_errors = {};
    if (this.checkout.status.error) {
      this.form_errors['transaction'] = {};
      for (const key in this.checkout.status.errors) {
        if (this.checkout.status.errors.hasOwnProperty(key)) {
          this.form_errors['transaction']['isInvalid'] =
            this.checkout.status.errors[key];
        }
      }

    } else {
      // if it is boleto, show link to payment
      // if it is creditCard, show status
      // this.checkout.status.result.date =
      // new Date(this.checkout.status.result.date)
      this.step = 4;
      // if it is debit...
    }
  }

}
