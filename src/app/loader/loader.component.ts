import { Component, OnInit, Output, Input } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  @Input('objText') objText: Object;

  public text: String = 'Carregando';

  constructor() { }

  ngOnInit() {
    if (this.objText) {
      Object.keys(this.objText).forEach(key => {
        this.text = this.objText[key] ? key : this.text;
      });
    }
  }

}
