import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignConfirmCancelSubscriptionComponent } from './confirm-cancel-subscription.component';

describe('CampaignConfirmCancelSubscriptionComponent', () => {
  let component: CampaignConfirmCancelSubscriptionComponent;
  let fixture: ComponentFixture<CampaignConfirmCancelSubscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignConfirmCancelSubscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignConfirmCancelSubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
