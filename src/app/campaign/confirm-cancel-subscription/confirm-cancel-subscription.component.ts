import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DonationService } from '../../services/donation.service';

@Component({
  selector: 'app-confirm-cancel-subscription',
  templateUrl: './confirm-cancel-subscription.component.html',
  styleUrls: ['./confirm-cancel-subscription.component.scss']
})
export class CampaignConfirmCancelSubscriptionComponent {

  loading = true;
  form_errors: any = {};
  status: any = {};

  constructor(
    private donationService: DonationService,
    private route: ActivatedRoute,
  ) {
    this.route.params.subscribe(params => {
      const campaignSlug = params['slug'];
      const transaction = params['transaction'];
      const token = params['token'];

      this.donationService.confirmCancelSubscription(
        campaignSlug,
        transaction,
        token,
      ).subscribe(data => {
        this.status = data;
        this.handleCancellationResult();
      }, error => {
        this.status = error.error;
        this.handleCancellationResult();
      });
    });
  }

  private handleCancellationResult() {
    this.form_errors = {...this.status.errors, ...this.status.message};
    if (this.status.error && !this.objectEmpty(this.form_errors)) {
      for (const error of Object.values(this.status.errors || {})) {
        this.form_errors.transaction = {isInvalid: error};
      }
    }
    this.loading = false;
  }

  objectEmpty(obj: any) {
    if (!obj) {
      return false;
    }
    return Object.keys(obj).length === 0;
  }
}
