import { ChangeDetectorRef, Component, ElementRef, EventEmitter, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { InvisibleReCaptchaComponent } from 'ngx-captcha';

import { Campaign } from '../../models/campaign';
import { DonationService } from '../../services/donation.service';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-cancel-subscription',
  templateUrl: './cancel-subscription.component.html',
  styleUrls: ['./cancel-subscription.component.scss']
})
export class CampaignCancelSubscriptionComponent {

  private static campaignAcquired: EventEmitter<Campaign> = new EventEmitter();

  @ViewChild('cancellationForm') form: NgForm;
  @ViewChild('recaptcha') recaptcha: InvisibleReCaptchaComponent;
  @ViewChild('submitButton') submitButton: ElementRef;

  public campaign: Campaign;
  public captcha = '';
  public email: string;
  public rubric: string;

  public form_errors: any = {};
  public step = 1;
  public status: any = {};

  constructor(
    private changeDetector: ChangeDetectorRef,
    private donationService: DonationService,
    private profileService: ProfileService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    this.route.params.subscribe(params => {
      const slug = params['slug'];
      this.donationService.getCampaignData(slug).subscribe(data => {
        CampaignCancelSubscriptionComponent.campaignAcquired.emit(data);
      }, error => {
        this.router.navigate(['campanha']);
      });
    });

    CampaignCancelSubscriptionComponent.campaignAcquired.subscribe((campaign: Campaign) => {
      this.campaign = campaign;
    });

    if (this.profileService.get()) {
      this.email = this.profileService.get().email;
    }
  }

  private formValidation() {
    this.form_errors = {};

    if (this.form.controls.email.errors) {
      if (this.form.controls.email.errors.required) {
        this.form_errors.email = {isEmpty: 'Email é campo obrigatório'};
      } else {
        this.form_errors.email = {isInvalid: 'Email inválido'};
      }
    }

    if (this.campaign.rubric && this.form.controls.rubric.errors) {
      this.form_errors.rubric = {isEmpty: 'É necessário selecionar um destino'};
    }

    return this.objectEmpty(this.form_errors);
  }

  verifyRobot() {
    if (this.formValidation()) {
      this.recaptcha.execute();
    }
  }

  markAsHuman(response: string) {
    this.captcha = response;
    this.changeDetector.detectChanges();
    this.cancelSubscription();
  }

  private cancelSubscription() {
    this.submitButton.nativeElement.disabled = true;

    if (!this.formValidation()) {
      this.submitButton.nativeElement.disabled = false;
      return false;
    }

    this.donationService.cancelSubscription({
      campaign: this.campaign,
      captcha: this.captcha,
      email: this.email,
      rubric: this.rubric,
    }).subscribe(data => {
      this.status = data;
      this.changeDetector.detectChanges();
      this.handleCancellationResult();
    }, error => {
      this.status = error.error;
      this.changeDetector.detectChanges();
      this.handleCancellationResult();
    });
  }

  private handleCancellationResult() {
    this.form_errors = {...this.status.errors, ...this.status.message};
    if (this.status.error && !this.objectEmpty(this.form_errors)) {
      for (const error of Object.values(this.status.errors || {})) {
        this.form_errors.transaction = {isInvalid: error};
      }
      this.submitButton.nativeElement.disabled = false;
    } else {
      this.step = 2;
    }
    this.changeDetector.detectChanges();
  }

  objectEmpty(obj: any) {
    if (!obj) {
      return false;
    }
    return Object.keys(obj).length === 0;
  }

}
