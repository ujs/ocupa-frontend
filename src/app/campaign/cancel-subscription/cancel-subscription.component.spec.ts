import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignCancelSubscriptionComponent } from './cancel-subscription.component';

describe('CampaignCancelSubscriptionComponent', () => {
  let component: CampaignCancelSubscriptionComponent;
  let fixture: ComponentFixture<CampaignCancelSubscriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignCancelSubscriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignCancelSubscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
