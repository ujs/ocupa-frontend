import { ChangeDetectorRef, Component, DoCheck, ElementRef, EventEmitter, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { InvisibleReCaptchaComponent } from 'ngx-captcha';

import { Address } from '../../models/address';
import { Campaign } from '../../models/campaign';
import { Profile } from '../../models/profile';
import StateUFs from '../../models/state-ufs';
import {
  PagSeguroCheckoutSession,
  Item,
  Payment,
  PaymentMethod,
  PaymentType
} from '../../models/checkout';
import { DonationService } from '../../services/donation.service';
import { ProfileService } from '../../services/profile.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AddressService } from '../../services/address.service';
import { Validators, NgForm } from '@angular/forms';

declare var PagSeguroDirectPayment: any;
@Component({
  selector: 'app-campaign-donation',
  templateUrl: './donation.component.html',
  styleUrls: ['./donation.component.scss']
})
export class CampaignDonationComponent implements DoCheck {
  public static campaignAcquired: EventEmitter<any> = new EventEmitter();
  public static cardOptionsAcquired: EventEmitter<any> = new EventEmitter();
  public static cardBrandAcquired: EventEmitter<any> = new EventEmitter();
  public static bankOptionsAcquired: EventEmitter<any> = new EventEmitter();
  public static cardTokenCreated: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('donation_form') form: NgForm;
  @ViewChild('send_payment') sendPaymentButton: ElementRef;

  public bank_options: Array<any>;
  public card_options: Array<any>;
  public regions: Array<any>;
  public cities: Array<any>;
  public alternative_value: number;
  public step = 1;
  public campaign: Campaign;

  public user: Profile;
  public checkout: PagSeguroCheckoutSession;
  public method = PaymentMethod;

  captcha = '';
  @ViewChild('recaptcha') recaptcha: InvisibleReCaptchaComponent;

  public form_errors = <any>{};

  public phoneMask = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public cpfMask = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];
  public dateMask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public cepMask = [/\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/];
  public expirationDateMask = [/\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];

  public PaymentType = PaymentType;

  constructor(
    private donationService: DonationService,
    private route: ActivatedRoute,
    private change: ChangeDetectorRef,
    private router: Router,
    private profileService: ProfileService,
    private addressService: AddressService,
  ) {

    this.route.params.subscribe(params => {
      const slug = params['slug'];
      this.donationService.getCampaignData(slug).subscribe(
        (data) => {
          CampaignDonationComponent.campaignAcquired.emit(data);
        }, (error) => {
          this.router.navigate(['campanha']);
        });
    });

    CampaignDonationComponent.campaignAcquired.subscribe((campaign: Campaign) => {
      this.campaign = campaign;
      this.checkout = new PagSeguroCheckoutSession(new Profile(), campaign.reference);

      if (this.campaign.rubric && this.campaign.rubric.length > 0) {
        this.checkout.rubric = this.campaign.rubric[0];
      }

      if (!this.profileService.get()) {
        this.profileService.set(new Profile());
      }

      this.checkout.sender = this.profileService.get();
      if (this.checkout.sender.birth_date && this.checkout.sender.birth_date.includes('-')) {
        this.checkout.sender.birth_date = new DatePipe('en-US')
          .transform(this.checkout.sender.birth_date, 'dd/MM/yyyy');
      }
      if (this.checkout.sender.cpf) {
        let i = 0;
        this.checkout.sender.cpf = '###.###.###-##'
          .replace(/#/g, _ => this.checkout.sender.cpf[i++]);
      }
      this.checkout.sender.phone = {
        areaCode: '',
        number: ''
      };
      this.checkout.campaign = campaign.slug;
      this.checkout.payment = new Payment();
      this.checkout.items.push(new Item(this.campaign.name,
        this.campaign.name, 0.00, 1));
      this.checkout.method = this.method.Credito;

      this.addressService.getRegions().subscribe((regions: any) => {
        this.regions = regions;
      }, error => {});

      this.donationService.getTokenFromServer().subscribe(res => {
        this.checkout.session_id = res.session;
        PagSeguroDirectPayment.setSessionId(this.checkout.session_id);

        PagSeguroDirectPayment.getPaymentMethods({
          amount: this.checkout.calculateAmount(),
          success: function (data) {
            if (data.paymentMethods.CREDIT_CARD) {
              CampaignDonationComponent.cardOptionsAcquired
                .emit(data.paymentMethods.CREDIT_CARD.options);
            }
            if (data.paymentMethods.ONLINE_DEBIT) {
              CampaignDonationComponent.bankOptionsAcquired
                .emit(data.paymentMethods.ONLINE_DEBIT.options);
            }
            return;
          },
          error: function (error) {
            CampaignDonationComponent.cardOptionsAcquired.error(error);
            return;
          },
        });
      }, error => {
        this.step = 0;
      });
    }, error => {});

    CampaignDonationComponent.cardTokenCreated.subscribe(token => {
      if ('error' in token) {
        this.form_errors = this.form_errors || {};
        this.form_errors['transaction'] = ['Erro ao criar transação com o PagSeguro'];
        this.sendPaymentButton.nativeElement.disabled = false;
        this.change.detectChanges();
      } else {
        this.checkout.card_token = token.card.token;
        this.checkout.payment.card_cvv = undefined;
        this.checkout.payment.card_number = undefined;
        this.checkout.payment.brand = undefined;
        this.sendTransaction();
      }
    }, error => {});

    CampaignDonationComponent.cardBrandAcquired.subscribe(brand => {
      if ('error' in brand) {
        this.form_errors = this.form_errors || {};
        this.form_errors['transaction'] = ['Erro na verificação do cartão de crédito (Verificar dados)'];
        this.sendPaymentButton.nativeElement.disabled = false;
        this.change.detectChanges();
      } else {
        this.checkout.payment.brand = brand.name;
        this.checkout.sender_hash = PagSeguroDirectPayment.getSenderHash();
        this.createCardToken();
      }
    }, error => {});

    CampaignDonationComponent.cardOptionsAcquired.subscribe(cards => {
      this.card_options = Object.values(cards);
    }, error => {});

    CampaignDonationComponent.bankOptionsAcquired.subscribe(banks => {
      this.bank_options = Object.values(banks);
    }, error => {});

  }

  ngDoCheck() {

  }

  step1Validation() {
    this.form_errors = {};

    if (!this.checkout.payment_type) {
      this.form_errors.payment_type = {isInvalid: 'É necessário selecionar o tipo de contribuição'};
    }

    if (!this.checkout.items[0].amount) {
      if (!this.isNumber(this.alternative_value)) {
        this.form_errors.value = {isInvalid: 'É necessário selecionar um valor de contribuição válido'};
      } else if (this.alternative_value < this.campaign.suggested_values[0]) {
        this.form_errors.value = {isInvalid: `É necessário selecionar um valor maior que R$ ${this.campaign.suggested_values[0]}`};
      }
    }

    if (this.campaign.rubric.length > 0 && this.form.controls.rubric.errors) {
      this.form_errors.rubric = {isEmpty: 'É necessário selecionar um destino'};
    }

    if (this.form.controls.name.errors) {
      this.form_errors.name = {isInvalid: 'Nome inválido'};
    }
    if (this.form.controls.email.errors) {
      if (this.form.controls.email.errors.required) {
        this.form_errors.email = {isEmpty: 'Email é campo obrigatório'};
      } else {
        this.form_errors.email = {isInvalid: 'Email inválido'};
      }
    }
    if (this.form.controls.holder_cpf.errors) {
      if (this.form.controls.holder_cpf.errors.required) {
        this.form_errors.holder_cpf = {isEmpty: 'CPF é campo obrigatório'};
      } else {
        this.form_errors.holder_cpf = {isInvalid: 'CPF inválido'};
      }
    }
    if (this.form.controls.phone_areacode.errors) {
      this.form_errors.phone_areacode = {isEmpty: 'DDD é campo obrigatório'};
    }
    if (this.form.controls.phone_number.errors) {
      if (this.form.controls.phone_number.errors.required) {
        this.form_errors.phone_number = {isEmpty: 'Telefone é campo obrigatório'};
      } else {
        this.form_errors.phone_number = {isInvalid: 'Número de telefone inválido'};
      }
    }

    if (this.objectEmpty(this.form_errors)) {
      this.step = 2;
    }
  }

  donationDataValidation(): boolean {
    this.step1Validation();

    if (this.checkout.method === PaymentMethod.Credito) {
      if (this.form.controls.holder_name.errors) {
        if (this.form.controls.holder_name.errors.pattern) {
          this.form_errors.holder_name = {isInvalid: 'Nome inválido, digite seu nome completo'};
        } else {
          this.form_errors.holder_name = {isEmpty: 'Nome é campo obrigatório'};
        }
      }
      if (this.form.controls.card_number.errors) {
        this.form_errors.card_number = {isEmpty: 'Número de cartão é campo obrigatório'};
      }
      if (this.form.controls.expiration_date.errors) {
        if (this.form.controls.expiration_date.errors.pattern) {
          this.form_errors.expiration_date = {isInvalid: 'Data incompleta (formato: MM/AAAA)'};
        } else {
          this.form_errors.expiration_date = {isEmpty: 'Data de expiração é campo obrigatório'};
        }
      }
      if (this.form.controls.card_cvv.errors) {
        if (this.form.controls.card_cvv.errors.minlength) {
          this.form_errors.card_cvv = {isInvalid: 'CVV incompleto'};
        } else {
          this.form_errors.card_cvv = {isEmpty: 'CVV é campo obrigatório'};
        }
      }
      if (this.form.controls.holder_birth_date.errors) {
        if (this.form.controls.holder_birth_date.errors.pattern) {
          this.form_errors.birth_date = {isInvalid: 'Data de nascimento inválida'};
        } else {
          this.form_errors.birth_date = {isEmpty: 'Data de nascimento é campo obrigatório'};
        }
      }
    }

    if (this.form.controls.postal_code.errors) {
      if (this.form.controls.postal_code.errors.pattern) {
        this.form_errors.postal_code = {isInvalid: 'CEP inválido'};
      } else {
        this.form_errors.postal_code = {isEmpty: 'CEP é campo obrigatório'};
      }
    }
    if (this.form.controls.state.errors) {
      this.form_errors.state = {isEmpty: 'Estado é campo obrigatório'};
    } else if (!this.checkout.payment.state_uf) {
      this.form_errors.state = {isInvalid: 'Preencha manualmente o estado'};
    }
    if (this.form.controls.city.errors) {
      this.form_errors.city = {isEmpty: 'Cidade é campo obrigatório'};
    }
    if (this.form.controls.district.errors) {
      this.form_errors.district = {isEmpty: 'Bairro é campo obrigatório'};
    }
    if (this.form.controls.street.errors) {
      this.form_errors.street = {isEmpty: 'Rua é campo obrigatório'};
    }
    if (this.form.controls.number.errors) {
      this.form_errors.number = {isEmpty: 'Número é campo obrigatório'};
    }

    return this.objectEmpty(this.form_errors);
  }

  verifyRobot() {
    if (this.donationDataValidation()) {
      this.recaptcha.execute();
    }
  }

  markAsHuman(response: string) {
    this.captcha = response;
    this.sendPayment();
  }

  get paymentOperation() {
    if (this.checkout.payment_type === PaymentType.Monthly) {
      return 'getPreApproval';
    } else {
      return 'sendTransaction';
    }
  }

  sendPayment() {
    this.sendPaymentButton.nativeElement.disabled = true;
    this.checkout.sender.cpf = this.checkout.payment.holder_cpf;
    this.checkout.sender.birth_date = this.checkout.payment.holder_birth_date;
    if (this.checkout.method === PaymentMethod.Credito) {
      this.checkout.sender.name = this.checkout.payment.holder_name;
    }
    if (!this.donationDataValidation()) {
      this.sendPaymentButton.nativeElement.disabled = false;
      return false;
    }
    if (this.checkout.method === PaymentMethod.Credito) {
      PagSeguroDirectPayment.getBrand({
        cardBin: this.checkout.payment.card_number,

        success: function (data) {
          CampaignDonationComponent.cardBrandAcquired.emit(data.brand);
        },
        error: function (error) {
          CampaignDonationComponent.cardBrandAcquired.emit(error);
        },
      });
    } else if (this.checkout.method === PaymentMethod.Boleto) {
      this.checkout.sender_hash = PagSeguroDirectPayment.getSenderHash();
      this.donationService.sendTransaction(this.checkout).subscribe(data => {
        this.checkout.status = data;
        this.change.detectChanges();
        this.handleDonationResult();
      }, error => {
        this.checkout.status = error.error;
        this.change.detectChanges();
        this.handleDonationResult();
      });
    }
  }

  createCardToken() {
    PagSeguroDirectPayment.createCardToken({
      cardNumber: this.checkout.payment.card_number,
      brand: this.checkout.payment.brand,
      cvv: this.checkout.payment.card_cvv,
      expirationMonth: this.checkout.payment.expiration_date.split('/')[0],
      expirationYear: this.checkout.payment.expiration_date.split('/')[1],
      success: function (data) {
        CampaignDonationComponent.cardTokenCreated.emit(data);
      },
      error: function (error) {
        CampaignDonationComponent.cardTokenCreated.emit(error);
      },
    });
  }

  sendTransaction() {
    this.donationService[this.paymentOperation](this.checkout).subscribe(data => {
      this.checkout.status = data;
      this.change.detectChanges();
      this.handleDonationResult();
    }, error => {
      this.checkout.status = error.error;
      this.change.detectChanges();
      this.handleDonationResult();
    });
    return;
  }

  onPaymentTypeChange() {
    if (this.checkout.payment_type !== PaymentType.Single) {
      this.checkout.method = PaymentMethod.Credito;
    }
  }

  setAlternativeValue() {
    const floatValue = parseFloat(this.alternative_value.toString());
    if (this.isNumber(this.alternative_value) && floatValue >= this.campaign.suggested_values[0]) {
      this.checkout.items[0].amount = floatValue;
    }
  }

  fetchAddress() {
    this.form_errors = {};
    if (this.form.controls.postal_code.errors) {
      this.form_errors['postal_code'] = {};
      if (this.form.controls.postal_code.errors.pattern) {
        this.form_errors['postal_code']['isInvalid'] = 'CEP inválido';
      }
      return;
    }
    if (!(this.checkout && this.checkout.payment && this.checkout.payment.postal_code)) {
      return;
    }

    const zipCode = this.checkout.payment.postal_code.replace('-', '');
    this.addressService
      .fetchAddress(zipCode)
      .subscribe((data: Address) => {
        const state_uf = data.uf;
        this.checkout.payment.state_uf = state_uf;
        const state = this.regions.filter((region) => {
          return region.alternate_names.indexOf(state_uf) >= 0;
        });
        if (state.length > 0) {
          this.checkout.payment.state = state[0].id;
          this.addressService
            .getCitiesByRegion(this.checkout.payment.state)
            .subscribe(cities => {
              this.cities = cities;
              const city = this.cities.filter((_city) => {
                return _city.name.indexOf(data.localidade) >= 0;
              });
              if (city.length > 0) {
                this.checkout.payment.city = city[0].id;
              }
            }, error => {});
        }
        this.checkout.payment.district = data.bairro;
        this.checkout.payment.street = data.logradouro;
      }, error => {});
  }

  getUF() {
    const state = this.regions.find(region => region.id === this.checkout.payment.state);
    this.checkout.payment.state_uf = StateUFs[state.name];
  }

  getCities() {
    this.getUF();
    this.addressService
      .getCitiesByRegion(this.checkout.payment.state)
      .subscribe(data => {
        this.cities = data;
      }, error => {});
  }

  private isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  get amount() {
    return this.checkout.calculateAmount();
  }

  get city() {
    if (this.cities && this.checkout.payment.city) {
      return this.cities.filter(city => {
        return city.id === this.checkout.payment.city;
      })[0].name;
    }
    return '';
  }

  get state() {
    if (this.checkout.payment.state_uf) {
      return this.checkout.payment.state_uf;
    }
    return '';
  }

  objectEmpty(obj) {
    if (!obj) {
      return false;
    }
    return Object.keys(obj).length === 0;
  }

  private handleDonationResult() {
    this.form_errors = {...this.checkout.status.errors, ...this.checkout.status.message};
    if (this.checkout.status.error && !this.objectEmpty(this.form_errors)) {
      this.form_errors['transaction'] = {};
      for (const error of Object.values(this.checkout.status.errors || {})) {
        this.form_errors['transaction']['isInvalid'] = error;
      }
      this.sendPaymentButton.nativeElement.disabled = false;
    } else {
      this.step = 4;
      // if it is debit...
    }
    this.change.detectChanges();
  }

}
