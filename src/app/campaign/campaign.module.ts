import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TextMaskModule } from 'angular2-text-mask';
import { NgxCaptchaModule } from 'ngx-captcha';

import { CampaignRoutingModule } from './campaign-routing.module';
import { FormErrorsModule } from '../form-errors/form-errors.module';
import { UsersModule } from '../users/users.module';

import { CampaignDonationComponent } from './donation/donation.component';
import { CampaignCancelSubscriptionComponent } from './cancel-subscription/cancel-subscription.component';
import { CampaignLoginComponent } from './login/login.component';
import { CampaignRecoverPasswordComponent } from './recover-password/recover-password.component';
import { CampaignRecoverPasswordConfirmComponent } from './recover-password-confirm/recover-password-confirm.component';
import { CampaignRegisterComponent } from './register/register.component';
import { CampaignSummaryComponent } from './summary/summary.component';
import { CampaignServiceUnavailableComponent } from './service-unavailable/service-unavailable.component';

import { environment } from '../../environments/environment';
import { CampaignConfirmCancelSubscriptionComponent } from './confirm-cancel-subscription/confirm-cancel-subscription.component';

@NgModule({
  imports: [
    CampaignRoutingModule,
    CommonModule,
    FormsModule,
    FormErrorsModule,
    TextMaskModule,
    UsersModule,
    NgxCaptchaModule.forRoot({
      invisibleCaptchaSiteKey: environment.invisibleCaptchaSiteKey
    }),
  ],
  declarations: [
    CampaignDonationComponent,
    CampaignCancelSubscriptionComponent,
    CampaignConfirmCancelSubscriptionComponent,
    CampaignLoginComponent,
    CampaignRecoverPasswordComponent,
    CampaignRecoverPasswordConfirmComponent,
    CampaignRegisterComponent,
    CampaignSummaryComponent,
    CampaignServiceUnavailableComponent,
  ]
})
export class CampaignModule { }
