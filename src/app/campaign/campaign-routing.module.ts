import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampaignDonationComponent } from './donation/donation.component';
import { CampaignCancelSubscriptionComponent } from './cancel-subscription/cancel-subscription.component';
import { CampaignConfirmCancelSubscriptionComponent } from './confirm-cancel-subscription/confirm-cancel-subscription.component';
import { CampaignLoginComponent } from './login/login.component';
import { CampaignRecoverPasswordComponent } from './recover-password/recover-password.component';
import { CampaignRecoverPasswordConfirmComponent } from './recover-password-confirm/recover-password-confirm.component';
import { CampaignRegisterComponent } from './register/register.component';
import { NotFoundComponent } from '../not-found/not-found.component';
import { CampaignSummaryComponent } from './summary/summary.component';
import { CampaignServiceUnavailableComponent } from './service-unavailable/service-unavailable.component';

const routes: Routes = [
  { path: 'campanha',
  children: [
    { path: '', component: NotFoundComponent },
    { path: 'registrar', component: CampaignRegisterComponent },
    { path: 'recuperar-senha', component: CampaignRecoverPasswordComponent },
    { path: 'recuperar-senha/:uid/:token', component: CampaignRecoverPasswordConfirmComponent },
    { path: 'sem-servico', component: CampaignServiceUnavailableComponent},
    { path: 'extrato/:transaction', component: CampaignSummaryComponent},
    { path: ':slug', component: CampaignLoginComponent },
    { path: ':slug/inscricao', component: CampaignDonationComponent},
    { path: ':slug/cancelar-inscricao', component: CampaignCancelSubscriptionComponent },
    { path: ':slug/cancelar-inscricao/:transaction/:token', component: CampaignConfirmCancelSubscriptionComponent },
  ]},
  { path: 'doe', redirectTo: '/campanha/doacoes/inscricao', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CampaignRoutingModule { }
