import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DonationService } from '../../services/donation.service';
import { PaymentMethod, PagSeguroCheckout } from '../../models/checkout';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class CampaignSummaryComponent implements OnInit {
  checkout: PagSeguroCheckout;
  public method = PaymentMethod;

  constructor(private donationService: DonationService,
    private route: ActivatedRoute, ) {
    this.route.params.subscribe(params => {
      const transaction = params['transaction'];
      this.donationService
        .getTransaction(transaction)
        .subscribe((checkout) => {
          this.checkout = checkout;
          console.log(this.checkout);
        }, error => {});
    });
  }

  ngOnInit() {
  }

  get city() {
    return this.checkout ? this.checkout.sender.city : '';
  }

  get state() {
    return this.checkout ? this.checkout.sender.state : '';
  }

  get amount() {
    return this.checkout ? this.checkout.value : 0.0;
  }

  get isBoletoPaid(): Boolean {
    return this.checkout ?
      (this.isBoleto && this.checkout.status === 3 ? true : false) :
      false;
  }

  get isBoleto(): Boolean {
    return this.checkout ?
      (this.checkout.payment_method === this.method.Boleto ? true : false) :
      false;
  }

  get payment_method() {
    return this.checkout ?
      (this.checkout.payment_method === 'boleto' ?
        'Boleto Bancário' : 'Cartão de crédito') :
      '';
  }

  get status() {
    return this.checkout ?
      (this.checkout.status === 3 ? 'Pago' : 'Não Pago') :
      '';
  }
}
