import { Component, OnInit, EventEmitter } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DonationService } from '../../services/donation.service';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-campaign-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class CampaignLoginComponent implements OnInit {
  public static campaignAcquired: EventEmitter<any> = new EventEmitter();

  constructor(private donationService: DonationService,
    private authService: AuthService,
    private route: ActivatedRoute,
    private profileService: ProfileService,
    private router: Router) {
    this.route.params.subscribe(params => {
      const slug = params['slug'];
      this.donationService.getCampaignData(slug).subscribe(
        (data) => {
          CampaignLoginComponent.campaignAcquired.emit(data);
        }, (error) => {
          this.router.navigate(['campanha']);
        });

    });

    CampaignLoginComponent.campaignAcquired.subscribe(campaign => {
      this.donationService.setCampaignSlug(campaign.slug);
      if (this.profileService.get()) {
        this.authService.destroy();
      }
      AuthService.loginSuccessNext = `/campanha/${ campaign.slug }/inscricao`;
    }, error => {});
  }

  ngOnInit() {
  }

}
