import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DonationService } from '../../services/donation.service';

@Component({
  selector: 'app-campaign-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class CampaignRegisterComponent implements OnInit {
  public campaignUrl;
  constructor(private router: Router,
    private donationService: DonationService) { }

  ngOnInit() {
    if(this.donationService.getCampaignSlug()) {
      this.campaignUrl = `/campanha/${ this.donationService.getCampaignSlug() }`;
    } else {
      this.router.navigate(['campanha']);
    }
  }

}
