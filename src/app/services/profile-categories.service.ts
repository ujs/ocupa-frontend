import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ActivismSchoolWorkplace, PoliticalFront, UJSManagerRole } from '../models/profile-categories';

@Injectable()
export class ProfileCategoriesService {

  constructor(private http: HttpClient) { }

  public getActivismSchoolWorkplace(): Observable<ActivismSchoolWorkplace[]> {
    return this.http.get<ActivismSchoolWorkplace[]>('/api/profile/activism-school-workplace');
  }

  public getPoliticalFronts(): Observable<PoliticalFront[]> {
    return this.http.get<PoliticalFront[]>('/api/profile/political-front');
  }

  public getUJSManagerRoles(): Observable<UJSManagerRole[]> {
    return this.http.get<UJSManagerRole[]>('/api/profile/ujs-manager-role');
  }
}
