import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { ProfileService } from './profile.service';
import { LocalStorageService } from 'ngx-webstorage';
import { Campaign } from '../models/campaign';

@Injectable()
export class DonationService {

  constructor(private http: HttpClient,
    private localStorageService: LocalStorageService,
    private profileService: ProfileService) {

  }

  getTokenFromServer() {
    const fullEndpointUrl = `${environment.apiUrl}/api/donation/session/`;

    return this.http.post(fullEndpointUrl, this.profileService.get()).map(
      (data: any) => {
        return data;
      }
    );
  }

  cancelSubscription(session) {
    const fullEndpointUrl = `${environment.apiUrl}/api/donation/pre-approvals/cancel/`;
    return this.http.post(fullEndpointUrl, session).map(data => data);
  }

  confirmCancelSubscription(campaignSlug: string, transactionCode: string, token: string) {
    const fullEndpointUrl = `${environment.apiUrl}/api/donation/pre-approvals/cancel/${transactionCode}/${token}/`;
    return this.http.get(fullEndpointUrl).map(data => data);
  }

  getPreApproval(session) {
    const fullEndpointUrl = `${environment.apiUrl}/api/donation/pre-approvals/subscribe/`;
    return this.http.post(fullEndpointUrl, session).map(data => data);
  }

  sendTransaction(session) {
    const fullEndpointUrl = `${environment.apiUrl}/api/donation/transaction/`;
    return this.http.post(fullEndpointUrl, session).map(
      (data: any) => {
        return data;
      }
    );
  }

  setCampaignSlug(slug) {
    this.localStorageService.store('campaignSlug', slug);
  }

  getCampaignSlug() {
    return this.localStorageService.retrieve('campaignSlug');
  }

  getCampaignData(slug) {
    const fullEndpointUrl = `${environment.apiUrl}/api/donation/campaigns/${slug}/`;
    return this.http.get(fullEndpointUrl).map(
      (data: Campaign) => {
        return data;
      }
    );
  }

  getTransaction(transaction) {
    const fullEndpointUrl = `${environment.apiUrl}/api/donation/${transaction}/`;
    return this.http.get(fullEndpointUrl).map(
      (data: any) => {
        return data;
      }
    );
  }

}
