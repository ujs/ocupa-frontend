import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

import { Address, City, Region } from '../models/address';

@Injectable()
export class AddressService {

  public static Error: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient) { }

  public getRegions(): Observable<Region[]> {
    return this.http.get<Region[]>(`/api/address/regions`, { headers: { ignoreLoadingBar: '' } });
  }

  public getCitiesByRegion(region_id): Observable<City[]> {
    return this.http.get<City[]>(`/api/address/cities?region=${region_id}`);
  }

  public getCityById(city_id: number): Observable<City> {
    return this.http.get<City>(`/api/address/cities/${city_id}`);
  }

  public getRegionById(region_id: number): Observable<Region> {
    return this.http.get<Region>(`/api/address/regions/${region_id}`);
  }

  public getRegionByUF(alternate_name: string): Observable<Region[]> {
    return this.http.get<Region[]>(`/api/address/regions?uf=${alternate_name}`);
  }

  public fetchAddress(zipCode: string): Observable<Address> {
    return this.http.get<Address>(`https://viacep.com.br/ws/${zipCode}/json/unicode/`);
  }
}
