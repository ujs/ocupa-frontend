import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { ProfileService } from './profile.service';

@Injectable()
export class OpinionService {

  constructor(private http: HttpClient,
    private profileService: ProfileService
  ) {}

  getConversations() {
    const fullEndpointUrl = `${environment.apiUrl}/api/conversations/`;

    return this.http.get(fullEndpointUrl).map(
      (data: any) => data
    );
  }

  getRandomComment(conversation: string) {
    const fullEndpointUrl = `${environment.apiUrl}/api/conversations/${conversation}/random_comment/`;

    return this.http.get(fullEndpointUrl).map(
      (data: any) => data
    );
  }

}
