import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { environment } from '../../environments/environment';
import { Profile } from '../models/profile';

@Injectable()
export class UJSAffiliationService {

  constructor(
    private http: HttpClient,
  ) {}

  create(profile: Profile, captcha: string): Observable<Profile> {
    const fullEndpointUrl = `${ environment.apiUrl }/api/profile/affiliation/`;
    return this.http.post<Profile>(fullEndpointUrl, Object.assign({token: captcha}, profile));
  }

  update(profile: Profile, captcha: string): Observable<Profile> {
    const fullEndpointUrl = `${ environment.apiUrl }/api/profile/${profile.id}/`;
    return this.http.put<Profile>(fullEndpointUrl, Object.assign({token: captcha}, profile));
  }
}
