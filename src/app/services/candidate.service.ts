import { Injectable, EventEmitter, Output  } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '../../environments/environment';
import { Candidate, SocialMedias } from '../models/candidate';

@Injectable()
export class CandidateService {

    candidates: Array<Candidate>;

    constructor(private http: HttpClient) {

        // VINICIUS BRASILINO
        const vinicius_social_media = new SocialMedias('viniciusfernandes09', 'viniciusbrasilino', 'vinicius.brasilinof@Gmail.com', '+5565981670560');
        const vinicius = new Candidate('Vinicius Brasilino', '/assets/images/candidates/vinicius.png', vinicius_social_media, 'Vinicius Brasilino,26, é jovem, negro e ator. Natural de Conceição de Almeida (BA), Vinícius se tornou mais tarde cidadão mato-grossense. Começou sua atuação política ainda no grêmio estudantil de sua escola e na UNEGRO. <br> Foi coordenador do DCE da Universidade Federal do Mato-Grosso, período em que lutou em defesa da universidade pública e contra a Emenda Constitucional 95 (PEC 95), que congela por 20 anos os investimentos do governo em educação e saúde.<br> Vinicius é presidente metropolitano da UJS MT e pré candidato a deputado estadual pelo PCdoB - MT.');

        // LEONARDO MATHEUS
        const leonardo_social_media = new SocialMedias('Leonardo.msr', 'LecoMatheus', 'leo.ujsdf@gmail.com', '+5561991759100');
        const leonardo = new Candidate('Leonardo Matheus', '/assets/images/candidates/leonardo.png', leonardo_social_media, 'Leonardo Matheus, 23, é nascido e criado na Ceilândia (DF) e estuda História na Universidade de Brasília (UnB). <br>Começou sua atuação no movimento social através do grêmio estudantil Honestino Guimarães, do Centro de Ensino Médio Elefante Branco, em Brasília. Léo foi um dos fundadores da União dos Estudantes Secundaristas do Distrito Federal (UESDF). Na presidência da entidade, liderou manifestações pelo passe-livre estudantil e pela ampliação dos investimentos em educação.<br>Estudante oriundo da periferia, faz parte do movimento HIP HOP na sua universidade ajuda a organizar a ação de ocupação cultural “Batalha na Escada.” Léo faz parte da direção executiva da UJS DF e é pré-candidato a deputado distrital pelo PCdoB - DF.');

        // Tayná Paolino
        const tayna_social_media = new SocialMedias('taynapaolino', 'taynapaolino', 'tayna.unirio@gmail.com', '+5521966875206');
        const tayna = new Candidate('Tayná Paolino ', '/assets/images/candidates/tayna.png', tayna_social_media, 'Tayná Paolino, 26, é da zona norte do Rio de Janeiro e mestranda em Ciência Política na Unirio.  Iniciou a atuação política aos 15 anos a partir do grêmio da sua escola, o Dom Pedro II. Foi presidente da AMES e participou do DCE da sua universidade. <br> Como presidente da União Estadual dos Estudantes do Rio, Tayná liderou a luta pelo passe-livre estudantil, pela transferência assistida dos estudantes da Universidade Gama Filho e UniverCidade e defendeu o investimento nas universidades estaduais, como a UENZO, a UERJ e a UENF.<br> Tayná Paolino atualmente preside a UJS municipal do Rio de Janeiro e é pré-candidata a deputada estadual pelo PCdoB - RJ.');

        // Luana Ramalho
        const luana_social_media = new SocialMedias('luanna.kathleen', 'luannaramalho1', 'luannaujs@gmail.com', '+5531992871533');
        const luana = new Candidate('Luana Ramalho ', '/assets/images/candidates/luana.png', luana_social_media, 'Luanna Ramalho, 21, é natural de Belo Horizonte e estuda direito no Centro Universitário Newton Paiva. Aos 16 anos, Luanna presidiu o grêmio do Colégio Estadual Central, escola tradicional de Belo Horizonte. Hoje, Luana preside a União Estadual dos Estudantes de Minas Gerais (UEE MG). No movimento estudantil universitário, colaborou na construção do Plano Estadual de Assistência Estudantil (PEAS), com a criação do Fundo Social do Minério e a retomada da sede histórica dos estudantes de minas. <br>Luanna é pré-candidata a deputada estadual pelo PCdoB-MG');

        // Fafá Capela
        const fafa_social_media = new SocialMedias('fafa.capela', 'fafacapela', 'fatimacapela.ufsc@gmail.com', '+5548998095711', 'fafacapela');
        const fafa = new Candidate('Fafá Capela', '/assets/images/candidates/fafa.png', fafa_social_media, 'Fafá Capela, 26, é natural de Florianópolis e graduada em ciências sociais pela Universidade Federal de Santa Catarina (UFSC), onde, atualmente, faz parte do programa de mestrado em sociologia política.<br>Fafá começou sua atuação no Diretório Central dos Estudantes (DCE) da Universidade Federal de Santa (UFSC). Nesse período, lutou pela democratização do acesso às universidades federais através de políticas afirmativas para estudantes negros, indígenas e oriundos de escola pública. <br>Tornou-se liderança do movimento feminista e tem como uma de suas principais pautas a defesa dos direitos das mulheres, da comunidade LGBT e de uma política a serviço da transformação. Esses motivos levaram Fátima Capela a ser pré-candidata a deputada estadual pelo PCdoB.');

        // Giovane Culau
        const giovane_social_media = new SocialMedias('giovaniculau', 'giovaniculau', 'giovaniculau@gmail.com', '+5551989458583', 'giovaniculau');
        const giovane = new Candidate('Giovani Culau', '/assets/images/candidates/giovane.png', giovane_social_media, 'Giovani Culau, 24, é um jovem e gaúcho de Porto Alegre presente nas lutas em defesa da educação. Possui uma trajetória muito semelhante a da pré candidata à Presidência da República Manuela D`Ávila (PCdoB). <br>Geovani faz parte do DCE da Universidade do Rio Grande do Sul (UFRGS) e foi vice-presidente regional da UNE no seu estado. Esteve na linha de frente das mobilizações contra as medidas de ataque à educação do governo Temer e liderou protestos que barraram a tentativa do prefeito de Porto Alegre de acabar com o meio-passe estudantil. <br>Giovani Culau é pré-candidato a deputado federal pelo PCdoB-RS.');

        // Germana Amaral
        const germana_social_media = new SocialMedias('germanaagenor', 'germanaujs', 'germanaagenor@gmail.com', '+5585996771154', 'AmaralGermana');
        const germana = new Candidate('Germana Amaral', '/assets/images/candidates/germana.png', germana_social_media, 'Germana Amaral, 24, é cearense de Fortaleza, militante das causas estudantis e feministas e cursa Gestão de Políticas Públicas na Universidade Federal do Ceará (UFC). <br>Germana fez parte do DCE da UFC e da diretoria da UNE. Esteve à frente das manifestações em defesa da democracia e liderou, junto aos jovens estudantes, a primavera feminista no Ceará. Germana Amaral  é pré-candidata a deputada estadual pelo Partido Comunista do Brasil (PCdoB) para defender os direitos da juventude, das mulheres, dos LGBTs e, sobretudo, para defender o Brasil. ');

        // Flor Ribeiro
        const flor = new Candidate('Flor Ribeiro', '/assets/images/candidates/flor.png', new SocialMedias(), 'Flor Ribeiro, 26, é jovem, mulher e atualmente reside em Cavaleiro - Jaboatão dos Guararapes, em Pernambuco. É graduada em enfermagem pela Universidade de Pernambuco (UPE), onde iniciou sua trajetória no movimento estudantil. <br>Flor presidiu o Diretório Central dos Estudantes (DCE) da UPE e a tradicional União dos Estudantes de Pernambuco, a UEP. Ligada aos movimentos culturais de periferia, lutou contra a redução da maioridade penal e por mais direitos sociais. Hoje, Flor Ribeiro é presidente da União da Juventude Socialista (UJS) de Pernambuco e pré candidata a deputada estadual pelo PCdoB.');

        // Henrique Domingues
        const henrique = new Candidate('Henrique Domingues', '/assets/images/candidates/henrique.png', new SocialMedias(), 'Henrique Domingues, 28, nasceu em Guarulhos (SP), é graduado em Logística Aeroportuária pela FATEC Guarulhos e acadêmico em Comércio Exterior na FATEC Zona Leste. <br>Henrique foi presidente do DCE da FATEC com mais de 10 mil votos, diretor da UEE SP e organizador do XIX Festival Mundial da Juventude e dos Estudantes. Liderou, também, a conquista de 6 mil novas vagas na FATEC e hoje é pré-candidato a deputado estadual pelo PCdoB-SP.');

        // Tamara Naiz
        const tamara = new Candidate('Tamara Naiz', '/assets/images/candidates/tamara.png', new SocialMedias(), 'Tamara Naiz, 29, é mestre em história econômica do Brasil, professora e pesquisadora.<br>Nascida e criada em Planaltina, milita desde a adolescência no movimento estudantil. Foi diretora da UNE, do Centro Popular da Mulher de Goiás (CPM) e presidiu a Associação Nacional de Pós-Graduandos (ANPG), período em que liderou conquistas importantes como a reserva de vagas em universidades federais para estudantes oriundos de escola pública, negros e indígenas.<br>Tamara Naiz é pré candidata a deputada distrital no Distrito Federal (DF).');

        // Dhemerson
        const dhemerson_social_media = new SocialMedias('DhemersonAdrie', 'dhemersonferreiraa', 'adriel_pvh12@hotmail.com', '+5569992742522', 'adrieldhemerson');
        const dhemerson = new Candidate('dhemerson Naiz', '/assets/images/candidates/dhemerson.png', dhemerson_social_media, 'Dhemerson Ferreira, 23, nasceu em Porto Velho (RO). É acadêmico do curso de administração de empresas na Faculdade Uniron, onde iniciou sua atuação política. Liderou mobilizações para barrar o aumento da tarifa do transporte coletivo para os estudantes, hoje fixada permanentemente no valor de R$ 1,00. <br>Dhemerson é vice-presidente da União da Juventude Socialista em Rondônia e pré-candidato a deputado estadual pelo PCdoB.');

        // Patrícia Santiago
        const patricia_social_media = new SocialMedias('patricia.santiago.522', 'patricasantiago', 'santiagopatricia65@gmail.com', '+5584999420401');
        const patricia = new Candidate('Patrícia Santiago', '/assets/images/candidates/patricia.png', patricia_social_media, 'Patrícia  Santiago, 23, é nascida na cidade de Carnaúbas (RN) e cursa Ciências Sociais na Universidade Federal do Rio Grande do Norte (UFRN). <br>Filha mais velha de deis irmãos, Patrícia é a primeira de sua família a ingressar no ensino superior. Iniciou sua militância política no movimento estudantil secundarista, quando presidiu o grêmio de sua escola. <br>Em 2016 participou pela primeira vez da disputa de um cargo eletivo, na ocasião como candidata a vereadora. Hoje, Patrícia é vice-presidente regional da UNE no RN e pré-candidata a deputada federal pelo PCdoB. ');

        // MC Carol
        const carol_social_media = new SocialMedias('mccaroldeniteroioficial', 'mccaroldeniteroioficial', '', '', 'mc_caroloficial');
        const carol = new Candidate('MC Carol', '/assets/images/candidates/carol.jpg', carol_social_media, 'Carolina Lourenço, mais conhecida como Mc Carol, 24 anos, nascida e criada na comunidade do Preventório em Niterói/Rio de Janeiro. <br>Mulher, jovem, negra e artista; <br>Ligada aos movimentos de mulheres e da cultura; cantando a realidade carioca através do funk, Carol, busca a transformação social através de uma política voltada para as mulheres, pobres e negros que assim como ela aspiram uma sociedade mais igualitária. <br>100% feminista filiou-se ao PCdoB e é pré candidata à deputada Estadual para garantir mais direitos sociais.');

        // Dani Balbi
        const dani_social_media = new SocialMedias('danielibaldi', 'danieli.balbi', 'danielibalbi@gmail.com');
        dani_social_media.site = 'docs.google.com/forms/d/12t-cqCn89AcuVuFiHLgU7NpnVuM8OZIjU0rlEugytsc';
        const dani = new Candidate('Dani Balbi', '/assets/images/candidates/dani.png', dani_social_media, 'Dani Balbi, 29, nasceu no Engenho da Rainha e atua profissionalmente como professora do Estado (SEEDUC) de jovens e adulto da Escola Politécnica da Fiocruz, no Rio de Janeiro. Negra e transexual, Dani tem uma trajetória de luta e muito esforço através da educação. <br>Estudou na Faetec de Quintino (Oscar Tenório), onde fez parte do grêmio e, depois, ingressou na Faculdade de Letras da UFRJ, onde foi fundadora e liderança do Movimento Quero-quero. Cursou graduação e mestrado.  Hoje é doutoranda e pesquisadora do CNPq em teatro brasileiro, afirmando a convicção de que a cultura e a ciência são capazes de emancipar o povo.');

        // CARINA VITRAL
        const carina_social_media = new SocialMedias(
            'carinavitral',
            'carinavitral',
            'carinavitral2018@gmail.com',
            '',
            'carinavitral',
            'www.carinavitral.umamais.com.br',
            'carinavitral'
        );
        const carina = new Candidate( 'Carina Vitral', '/assets/images/candidates/carina.png', carina_social_media, 'Carina Vitral, 29, nasceu em Santos (SP), estuda Economia na PUC de São Paulo e iniciou sua atuação política ao participar do movimento estudantil. Foi presidenta da União Estadual dos Estudantes (UEE-SP) entre 2013 e 2015, quando participou ativamente da conquista do passe-livre para os estudantes paulistas. De 2015 a 2017, presidiu a União Nacional dos Estudantes (UNE), período em que estudantes universitários fizeram ocupações em defesa das universidades públicas e resistiram à Proposta de Emenda Constitucional (PEC 95) que congela por 20 anos os investimentos do governo federal em educação e saúde. Atualmente, preside a União da Juventude Socialista (UJS). Carina concorreu à prefeitura de Santos, ficando em segundo lugar na disputa e hoje é pré-candidata a deputada estadual pelo PCdoB.');

        // SAMUEL OLIVEIRA
        const samuel_social_medias = new SocialMedias('samueloliveira65', 'samueloliveira65', '65samueloliveira@gmail.com', '+5511979647088', '', 'www.samueloliveira65.com.br');
        const samuel = new Candidate('Samuel', '/assets/images/candidates/samuel.png', samuel_social_medias, 'Samuel Oliveira, um jovem engajado na luta política. Ingressou nos movimentos sociais ainda no ensino médio, quando estudava na E. E. Prof. Moacyr Campos (MOCAM) na Zona Leste de São Paulo. Participou de manifestações em defesa da educação pública, dos estudantes e dos professores. <br> <br> Em 2016, aos 19 anos, participou das eleições pela primeira vez como candidato a vereador na cidade de São Paulo. Dentre suas principais pautas, está a luta pela renovação política e a melhoria da educação pública para todos. Samuel Oliveira é pré-candidato a deputado estadual pelo PCdoB em São Paulo.');

        const yann_social_medias = new SocialMedias(
            'YannEvannovick',
            'yannevanovick',
            'yannjuventude@gmail.com',
            '+5592993340202',
            'YannAmazonas',
        );
        const yann = new Candidate(
            'Yann Evanovick',
            '/assets/images/candidates/yann.jpg',
            yann_social_medias,
            'Yann Evanovick* Nasceu em Manaus (AM), estuda História na ' +
            'Universidade Federal do Amazonas e iniciou sua atuação política ' +
            'ao participar do movimento estudantil. Foi presidente da União ' +
            'Municipal dos Estudantes Secundaristas (UMES-Manaus) entre 2007 ' +
            'e 2008, período em que tramitou na Câmara Municipal, o projeto ' +
            'de lei que propunha a redução da meia-passagem para os ' +
            'estudantes, o que motivou grandes passeatas na cidade, ' +
            'lideradas por Yann e outros jovens.Com a repercussão, em 2009 ' +
            'foi eleito o segundo amazonense a ocupar a cadeira de ' +
            'presidente da União Brasileira dos Estudantes Secundaristas ' +
            '- UBES.<br>' +
            'Na UBES, passou a ter diálogo com o Congresso Nacional ' +
            'Brasileiro e até mesmo com o então Presidente da República na ' +
            'época - Lula e depois Dilma. Nesse período foi aprovado o Plano ' +
            'Nacional de Educação, a destinação de 50% do Pré-sal para a ' +
            'Educação, a PEC da Juventude, o Programa Nacional de Acesso ao ' +
            'Ensino Técnico e Emprego - PRONATEC, o Fim do Fiador no Fies, ' +
            'dentre outras conquistas.<br>' +
            'Ao retornar para o Amazonas, foi candidato a vereador em 2012 e ' +
            'a Deputado Estadual em 2014, em 2016 foi candidato a ' +
            'Vice-Prefeito de Manaus. Atualmente é membro da Direção ' +
            'Nacional da União da Juventude Socialista - UJS, além de fazer ' +
            'parte da Direção Estadual e Municipal do PCdoB, partido pelo ' +
            'qual é Pré – candidato à Deputado Federal.',
        );

        this.candidates = [
            carina,
            carol,
            dani,
            dhemerson,
            fafa,
            flor,
            germana,
            giovane,
            henrique,
            leonardo,
            luana,
            patricia,
            samuel,
            tayna,
            tamara,
            vinicius,
            yann,
        ];
    }

    getCandidates(): Observable<Candidate[]> {
        // FOR WHEN WE HAVE AN API
        // return this.http.get<Candidate[]>('/api/candidates');

        const obsCandidates = new Observable<Candidate[]>((observer) => {
            observer.next(this.candidates);
            observer.complete();
        });

        return obsCandidates;
    }
}
