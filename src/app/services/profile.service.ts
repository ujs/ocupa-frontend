import { Injectable, EventEmitter, Output  } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '../../environments/environment';
import { Profile, ProfileStatus } from '../models/profile';

@Injectable()
export class ProfileService {

  @Output() static profileChange: EventEmitter<Profile> = new EventEmitter(true);

  constructor(private http: HttpClient,
              private localStorageService: LocalStorageService) {}

  save(profile: Profile): Observable<Profile> {
    let localProfile = new Profile();
    localProfile = Object.assign(localProfile, profile);
    localProfile.image = undefined;

    const fullEndpointUrl = `${environment.apiUrl}/api/profile/${localProfile.id}/`;
    return this.http.put<Profile>(fullEndpointUrl, localProfile);
  }

  saveImage(form) {
    const formData = new FormData();
    formData.append('image', form.imageFile);

    const fullEndpointUrl = `${environment.apiUrl}/api/profile/${this.get().id}/image/`;

    return this.http.post<Profile>(fullEndpointUrl, formData);
  }

  changePassword(form: any) {
    const data = {};
    data['new_password1'] = form.password;
    data['new_password2'] = form.passwordConfirmation;
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/password/change/`;
    return this.http.post<Profile>(fullEndpointUrl, data);
  }

  set(profile: Profile): Profile {
    this.localStorageService.store('currentProfile', profile);
    ProfileService.profileChange.emit(profile);
    return profile;
  }

  get(): Profile {
    return this.localStorageService.retrieve('currentProfile');
  }

  updateFromServer(): Observable<Profile> {
    const fullEndpointUrl = `${environment.apiUrl}/api/profile/me/`;
    return this.http.get(fullEndpointUrl).map(profile => {
      return this.set(<Profile>profile);
    });
  }

  computePersonalProgress(profile: Profile): number {
    let all_fields = 10;
    let complete_fields = 0;

    if (profile.name) {
      complete_fields++;
    }
    if (profile.race) {
      complete_fields++;
    }
    if (profile.gender) {
      complete_fields++;
    }
    if (profile.birth_date) {
      complete_fields++;
    }
    if (profile.biography) {
      complete_fields++;
    }
    if (profile.address) {
      complete_fields++;
    }
    if (profile.address_number) {
      complete_fields++;
    }
    if (profile.zipcode) {
      complete_fields++;
    }
    if (profile.phone_number) {
      complete_fields++;
    }
    if (profile.studies !== null) {
      complete_fields++;
      if (profile.studies) {
        all_fields++;
        if (profile.school) {
          complete_fields++;
        }
      }
    }
    if (profile.is_working !== null) {
      complete_fields++;
      if (profile.is_working) {
        all_fields++;
        if (profile.workplace) {
          complete_fields++;
        }
      }
    }
    return complete_fields / all_fields;
  }

  computeActivismProgress(profile: Profile): number {
    let all_fields = 4;
    let complete_fields = 0;
    if (profile.activism_place) {
      complete_fields++;
    }
    if ((profile.activism_school_workplace && profile.activism_school_workplace.length > 0) ||
      (profile.activism_school_workplace_other)) {
      complete_fields++;
    }
    if (profile.has_mandate !== null) {
      complete_fields++;
      if (profile.has_mandate) {
        all_fields++;
        if (profile.mandate) {
          complete_fields++;
        }
      }
    }
    if (profile.has_vote !== null) {
      complete_fields++;
      if (profile.has_vote) {
        all_fields++;
        if (profile.vote_city) {
          complete_fields++;
        }
      }
    }
    return complete_fields / all_fields;
  }

  computeFrontsProgress(profile: Profile): number {
    const all_fields = 2;
    let complete_fields = 0;
    if ((profile.political_front && profile.political_front.length > 0) ||
      (profile.political_front_other)) {
      complete_fields++;
    }
    if (profile.ujs_manager_role && profile.ujs_manager_role.length > 0) {
      complete_fields++;
    }
    return complete_fields / all_fields;
  }

  isFilled(profile: Profile): boolean {
    if (this.computePersonalProgress(profile) >= 0.7
     || this.computeActivismProgress(profile) >= 0.7
     || this.computeFrontsProgress(profile) >= 0.7) {
       return true;
     }
     return false;
  }

  getConnectedAccounts() {
    const fullEndpointUrl = `${environment.apiUrl}/api/profile/socialaccounts`;
    return this.http.get(fullEndpointUrl).map(
        data => {
          return data;
        },
        error => error
      );
  }


}
