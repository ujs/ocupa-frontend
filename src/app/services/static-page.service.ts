import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { StaticPage } from '../models/static-page';

@Injectable()
export class StaticPagesService {

  constructor(private http: HttpClient) { }

  get(name: string): Observable<StaticPage[]> {
    return this.http.get<StaticPage[]>(`/api/pages/?url_prefix=/${name}/`);
  }
}
