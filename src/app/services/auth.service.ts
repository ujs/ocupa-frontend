import { Injectable, EventEmitter, Output  } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { Profile } from '../models/profile';
import { ProfileService } from './profile.service';

import { environment } from '../../environments/environment';

import { LocalStorageService } from 'ngx-webstorage';


@Injectable()
export class AuthService {

  public static loginSuccess: EventEmitter<any> = new EventEmitter<any>();
  public static loginSuccessNext: string;
  public static logout: EventEmitter<any> = new EventEmitter<any>();
  public static connectSuccess: EventEmitter<any> = new EventEmitter<any>();

  constructor(private http: HttpClient,
              private profileService: ProfileService,
              private router: Router,
              private localStorageService: LocalStorageService) {

    // Listen for auth related events and prepare reactions to it
    // Call this event after a sucessful login
    AuthService.loginSuccess.subscribe(data => {
      const token: string = this.setToken(data['key']);
      this.profileService.updateFromServer().subscribe(
        profile => {
          // If loginSuccessNext is set, use it to decide the next route
          if (AuthService.loginSuccessNext) {
            this.router.navigateByUrl(AuthService.loginSuccessNext);
          } else if (!this.profileService.isFilled(profile)) {
            // If user has filled a percentage of profile data smaller than threshold, redirects to profile edition
            this.router.navigateByUrl('perfil/editar');
          } else {
            // Otherwise, go with the default
            this.router.navigateByUrl('');
          }
        },
        error => {});
    }, error => {});

    // Call this event to trigger a complete logout
    AuthService.logout.subscribe(() => {
      this.destroy();
      this.router.navigate(['']);
    });
  }

  signIn(form: any) {
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/login/`;
    return this.http.post(fullEndpointUrl, form).map(
        data => {
          return AuthService.loginSuccess.emit(data);
        },
        error => error
      );
  }

  passwordReset(form: any) {
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/password/reset/`;
    return this.http.post(fullEndpointUrl, form);
  }

  passwordResetConfirm(form: any) {
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/password/reset/confirm/`;
    return this.http.post(fullEndpointUrl, form);
  }

  signInFacebook(accessToken: string) {
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/facebook/`;
    return this.http.post(fullEndpointUrl, {access_token: accessToken}).map(
        data => {
          AuthService.loginSuccess.emit(data);
          return data;
        },
        error => error
      );
  }

  connectFacebook(accessToken: string) {
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/facebook/connect/`;
    return this.http.post(fullEndpointUrl, {access_token: accessToken}).map(
        data => {
          AuthService.connectSuccess.emit(data);
          return data;
        },
        error => {
          AuthService.connectSuccess.emit(error);
        }
      );
  }

  requestTwitterAccessToken(oauth_token:string, oauth_verifier: string){
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/twitter/access_token/`;
    return this.http.post(fullEndpointUrl, {oauth_token: oauth_token, oauth_verifier: oauth_verifier}).map(
      data => {
        return data;
      },
      error => error
    );
  }

  requestTwitterRequestToken() {
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/twitter/request_token/`;
    return this.http.post(fullEndpointUrl, {}).map(
        data => {
          return data;
        },
        error => error
      );
  }

  connectTwitter(accessToken: string, tokenSecret: string) {
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/twitter/connect/`;
    return this.http.post(fullEndpointUrl, {access_token: accessToken, token_secret: tokenSecret}).map(
        data => {
          AuthService.connectSuccess.emit(data);
          return data;
        },
        error => {
          AuthService.connectSuccess.emit(error);
        }
      );
  }

  signUp(form: any): Observable<any> {
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/registration/`;
    return this.http.post(fullEndpointUrl, form).map(
      data => data,
      error => error
    );
  }

  saveUser(form: any): Observable<any> {
    const fullEndpointUrl = `${environment.apiUrl}/api/profile/register_user/`;
    return this.http.post(fullEndpointUrl, form).map(
      data => {
        return data;
      },
      error => error
    );
  }

  verifyEmail(form: any): Observable<any> {
    const fullEndpointUrl = `${environment.apiUrl}/rest-auth/registration/verify-email/`;
    return this.http.post(fullEndpointUrl, form).map(
      data => {
        return AuthService.loginSuccess.emit(data);
      },
      error => error
    );
  }


  getTokenFromServer() {
    const fullEndpointUrl = `${environment.apiUrl}/api/profile/key/`;
    return this.http.get(fullEndpointUrl).map(
      (data: any) => {
        return data;
      }
    );
  }

  getAffiliationUserStatus(email: string) {
    const fullEndpointUrl = `${environment.apiUrl}/api/profile/user_status/`;
    return this.http.post(fullEndpointUrl, {'email' : email, 'congress_subscription': true}).map(
      (data: any) => {
        return data;
      }
    );
  }

  getUserStatus(email: string) {
    const fullEndpointUrl = `${environment.apiUrl}/api/profile/user_status/`;
    return this.http.post(fullEndpointUrl, {'email' : email}).map(
      (data: any) => {
        return data;
      }
    );
  }

  destroy() {
    this.localStorageService.clear('currentProfile');
    this.localStorageService.clear('token');
  }

  setToken(token: string): string {
    this.localStorageService.store('token', token);
    return token;
  }

  getToken(): string {
    return this.localStorageService.retrieve('token');
  }

}
