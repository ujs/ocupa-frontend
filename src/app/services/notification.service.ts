import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from 'ngx-webstorage';

import { environment } from '../../environments/environment';
import { ProfileService } from './profile.service';
import { NotificationInfo } from '../models/notification-info';


@Injectable()
export class NotificationService {

  constructor (private http: HttpClient, private localStorageService: LocalStorageService,
    private profileService: ProfileService) {

    // Listen for profile changes
    ProfileService.profileChange.subscribe(profile => {
      // Save the email as a hash in the onesignal profile
      if (profile) {
        this.sendEmail(profile.email);

        // Save the onsesignal userId, if available, in the API backend
        const notificationInfo = this.getInfo();
        if (!this.insideIframe && notificationInfo) {
          this.sendOneSignalId(notificationInfo.oneSignalAppId);
        }
      }
    }, error => {});
  }

  saveInfo(info: NotificationInfo): void {
    this.localStorageService.store('notificationInfo', info);
  }

  getInfo(): NotificationInfo {
    return this.localStorageService.retrieve('notificationInfo');
  }

  sendEmail(email: string) {
    // get a reference to the OneSignal SDK initialized on the index.html
    const OneSignal = window['OneSignal'] || [];

    if (!this.insideIframe) {
      OneSignal.push(function() {
        OneSignal.syncHashedEmail(email);
        OneSignal.setEmail(email);
      });
    }
  }

  sendOneSignalId(notificationId: string) {
    const fullEndpointUrl = `${environment.apiUrl}/api/onesignal/profile/`;

    return this.http.post<NotificationInfo>(fullEndpointUrl, {
      onesignal_id: notificationId
    }).subscribe(data => {}, error => {});
  }

  private get insideIframe(): boolean {
    try {
      return window.self !== window.top;
    } catch {
      return true; // Probably CORS problem.
    }
  }

}
