import { Injectable } from '@angular/core';

declare var require: any;

@Injectable()
export class CPFValidationService {

  private cpf: any;

  constructor() {
    this.cpf = require('gerador-validador-cpf');
  }

  validate(cpf: string): boolean {
    if (cpf == null) {
      return false;
    }
    return this.cpf.validate(cpf);
  }
}
