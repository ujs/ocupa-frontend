import { Injectable, EventEmitter } from '@angular/core';
import { AuthService } from './auth.service';
import { environment } from '../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class SocialTwitterService {
  public loginReturn: EventEmitter<any> = new EventEmitter<any>();
  public connectReturn: EventEmitter<any> = new EventEmitter<any>();
  public tokenReturn: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private http: HttpClient,
    private authService: AuthService,
  ) { }

  login() { }

  getApiToken() {
    this.authService.requestTwitterRequestToken().subscribe((response: any) => {
      if(response.oauth_token) {
        this.tokenReturn.emit(response.oauth_token);
      } else {
        this.tokenReturn.emit('Houve um erro ao se conectar ao Twitter');
      }
    }, (error: any) => {
      this.connectReturn.emit(error)
    });
  }

}
