import { TestBed, inject } from '@angular/core/testing';

import { CPFValidationService } from './cpf-validation.service';

describe('CPFValidationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CPFValidationService]
    });
  });

  it('should be created', inject([CPFValidationService], (service: CPFValidationService) => {
    expect(service).toBeTruthy();
  }));
});
