import { Component } from '@angular/core';

@Component({
  selector: 'app-verify-email-page',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailPageComponent { }
