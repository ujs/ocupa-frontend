import { Component } from '@angular/core';

@Component({
  selector: 'app-recover-password-page',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordPageComponent { }
