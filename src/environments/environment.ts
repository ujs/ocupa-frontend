// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  sentryDSN: '',
  apiUrl: '',
  facebookAppId: '236375206936856',
  onSignalAppId: 'c0c4b73d-c2cf-4e79-97e5-69ba68e0d171',
  invisibleCaptchaSiteKey: '6LeRilUUAAAAAIeRFw9lKqSb7sN4by28y39dYd95'
};
